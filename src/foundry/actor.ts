import { IItem } from "./item";

export interface IActor {
    _id: string;
    name: string;
    img: string;
    type: 'hazard'|'npc';
    data: object;
    items: IItem[];
    folder: string;
}

export class Actor<T> {
    _id: string;
    name: string = '';
    img: string = 'icons/svg/mystery-man.svg';
    type: 'hazard'|'npc';
    data: T;
    items: IItem[] = [];
    folder: string;
}

export class SaveDetails {
    value: number = 0;
    saveDetail: string = "";
}

export class AbilityDetails {
    value: number;
    mod: number;
}