export class ModulePackInfo {
    name: string;
    label: string;
    path: string;
    entity: string;
    module: string;
}

export class ModuleInfo {
    name: string;
    title: string;
    description: string;
    version: string;
    author: string;
    minimumCoreVersion: string;
    compatibleCoreVersion: string;
    packs?: ModulePackInfo[];
}