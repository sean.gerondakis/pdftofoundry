export interface PermissionInfo {
    [user: string]: number;
}

export class Journal {
    _id: string;
    name: string;
    permission: PermissionInfo;
    flags: any;
    content: string;
    img?: string;
    folder?: string;

    constructor(name: string, content: string, img?: string) {
        this.name = name;
        this.permission = { "default": 0 };
        this.flags = {};
        this.content = content;
        this.img = img;
    }
}