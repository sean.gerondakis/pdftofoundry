export * from './module'
export * from './journal'
export * from './actor'
export * from './hazard'
export * from './npc'
export * from './item'

export class Scene {
    _id: string;
    name: string;
    description: string;
    img: string;
    width: number;
    height: number;
    grid: number;
    shiftX: number;
    shiftY: number;
    folder: string;
}
