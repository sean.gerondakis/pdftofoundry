import * as PDFJS from "pdfjs-dist";

import FileSaver = require("file-saver");
import JSZip = require("jszip");
import rangeParser from 'parse-numeric-range';

require('./style.scss');

import * as Foundry from '../foundry'
import { parsePdf, TextDetail, PdfData, PdfImage } from '../parse'
import adventures from '../modules'
import { ModuleDescription, ModuleData } from "../modules/IAdventure";
import { ModuleImporter } from "../modules/ModuleImporter";
import { IModuleOutput } from "../IModuleOutput";

const urlParams = new URLSearchParams(window.location.search);

const pages = urlParams.get('pages') !== null ? rangeParser(urlParams.get('pages'), 10) : undefined;

var pdfPath : string = urlParams.get('pdfPath') !== null ? urlParams.get('pdfPath') : undefined;
var forceAdventure : number = urlParams.get('forceAdventure') !== null ? parseInt(urlParams.get('forceAdventure'), 10) : undefined;

console.clear();


PDFJS.GlobalWorkerOptions.workerSrc = "./pdf.worker.bundle.js";

function progressFn(status: string, progress: number) {
    const dropAreaText = document.getElementById('dropLocationText');
    dropAreaText.innerText = status;

    const dropAreaProgress = document.getElementById('progressBar') as HTMLProgressElement;
    dropAreaProgress.value = progress;

    //console.log(status);
}

function makeid() {
    var result           = '';
    var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for ( var i = 0; i < 16; i++) {
       result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
}

class WebModuleOutput implements IModuleOutput {
    prefix: string;
    actors: Foundry.IActor[] = [];
    items: Foundry.IItem[] = [];
    journals: Foundry.Journal[] = [];
    scenes: Foundry.Journal[] = [];

    constructor(prefix: string) {
        this.prefix = prefix;
    }

    progressFn(status: string, progress: number) {
        progressFn(status, progress);
    }

    async emitFile(path: string, name: string, data: string | Blob) {
        zip.file(name, data);
        return `${path}/${name}`;
    }

    async createScene(scene: any) {
        scene._id = makeid();
        this.scenes.push(scene);        
    }
    
    async createJournal(journal: Foundry.Journal) {
        journal._id = makeid();
        this.journals.push(journal);
    }

    async createActor(actor: Foundry.IActor) {
        actor._id = makeid();
        this.actors.push(actor);
    }

    async createItem(item: Foundry.IItem) {
        item._id = makeid();
        this.items.push(item);
    }

    finalize() {
        zip.file(this.prefix + '/packs/journals.db', this.journals.map(e => JSON.stringify(e) + '\n').reduce((c, x) => c + x, ''));
        zip.file(this.prefix + '/packs/actors.db', this.actors.map(e => JSON.stringify(e) + '\n').reduce((c, x) => c + x, ''));
        zip.file(this.prefix + '/packs/scenes.db', this.scenes.map(e => JSON.stringify(e) + '\n').reduce((c, x) => c + x, ''));
        zip.file(this.prefix + '/packs/items.db', this.items.map(e => JSON.stringify(e) + '\n').reduce((c, x) => c + x, ''));
        //createModuleJson();
    }
}

var zip = new JSZip();

function createModuleJson(adventureModule: ModuleDescription) {
    let moduleJson : Foundry.ModuleInfo = {
        "name": adventureModule.prefix,
        "title": adventureModule.fullName,
        "description": "",
        "version": "1.0.0",
        "author": "fryguy",
        "minimumCoreVersion": "0.4.7",
        "compatibleCoreVersion": "0.5.5",
        "packs": [
            {
                "name": "journals",
                "label": `${adventureModule.fullName} Journals`,
                "path": "packs/journals.db",
                "entity": "JournalEntry",
                "module": adventureModule.prefix
            },
            {
                "name": 'scenes',
                "label": `${adventureModule.fullName} Scenes`,
                "path": 'packs/scenes.db',
                "entity": "Scene",
                "module": adventureModule.prefix
            },
            {
                "name": 'actors',
                "label": `${adventureModule.fullName} Scenes`,
                "path": 'packs/actors.db',
                "entity": "Actor",
                "module": adventureModule.prefix
            }
        ]
    };

    zip.file(adventureModule.prefix + '/module.json', JSON.stringify(moduleJson));
}

function copyToClipboard(text: string) {
    const input = document.createElement('input') as HTMLInputElement;
    input.type = 'text';
    input.value = text;
    
    document.body.appendChild(input);
    input.select();
    document.execCommand('copy');
    document.body.removeChild(input);
}

function IsPotentialImage(image: PdfImage) {
    if (image.width == 2550 && image.height == 1669) { return false; }
    if (image.width == 1261 && image.height == 551) { return false; }
    if (image.width == 1261 && image.height == 418) { return false; }
    if (image.width == 1258 && image.height == 563) { return false; }
    if (image.width == 218 && image.height == 122) { return false; }
    if (image.width == 540 && image.height == 183) { return false; }
    if (image.width == 780 && image.height == 436) { return false; }
    if (image.width == 1299 && image.height == 155) { return false; }
    if (image.width == 1299 && image.height == 174) { return false; }
    if (image.width == 2555 && image.height == 1674) { return false; }
    if (image.width == 461 && image.height == 555) { return false; }
    if (image.width == 296 && image.height == 1096) { return false; }
    if (image.width == 1273 && image.height == 1674) { return false; }
    if (image.width == 1271 && image.height == 1637) { return false; }
    if (image.width == 1262 && image.height == 1637) { return false; }
    if (image.width == 1260 && image.height == 175) { return false; }
    if (image.width == 1265 && image.height == 175) { return false; }
    if (image.width == 1260 && image.height == 194) { return false; }
    if (image.width == 238 && image.height == 618) { return false; }
    if (image.width < 150 || image.height < 150) { return false; }

    return true;
}

async function generateModule(pdfPath: string) {
    let parsedPdf : PdfData;
    if (pdfPath.indexOf('.json') !== -1) {
        let data = await fetch(pdfPath);
        parsedPdf = {
            textDetails: await data.json(),
            imageDetails: []
        };
        if (pages !== undefined)
            parsedPdf.textDetails = parsedPdf.textDetails.filter(x => pages.includes(x.page));
    } else {
        parsedPdf = await parsePdf(pdfPath, progressFn, pages);
    }

    const pageContainer = document.getElementById('pageContainer');
    const pageSelector = document.getElementById('pageSelector') as HTMLSelectElement;

    async function updateDebug() {
        pageContainer.innerHTML = '';
        if (pageSelector.value == 'Journals') {
            if (output !== undefined) {
                for (let entry of output.journals) {
                    pageContainer.innerHTML += `<h1>${entry.name}</h1> <p>${entry._id}</p> ${entry.content} <hr />`;
                }
            }
        } else if (pageSelector.value == 'Actors') {
            if (output !== undefined) {
                for (let scene of output.scenes) {
                    pageContainer.innerHTML += `<h1>${scene.name}</h1> <pre>${JSON.stringify(scene, null, 4)}</pre> <hr />`;
                }
        
                for (let actor of output.actors) {
                    pageContainer.innerHTML += `<h1>${actor.name}</h1> <p>${actor._id}</p> <pre>${JSON.stringify(actor, null, 4)}</pre> <hr />`;
                }
        
                for (let item of output.items) {
                    pageContainer.innerHTML += `<h1>${item.name}</h1> <p>${item._id}</p> <pre>${JSON.stringify(item, null, 4)}</pre> <hr />`;
                }
            }
        } else if (pageSelector.value == 'Layout') {
            if (output !== undefined && adventureModule !== undefined) {
                for (const pageNum of [...new Set(parsedPdf.textDetails.map(t => t.page))]) {
                    const layout = adventureModule.detectPageLayout(parsedPdf, pageNum);
                    if (layout)
                        pageContainer.innerHTML += `${layout},<br>`;
                    else
                        pageContainer.innerHTML += `<br>`;
                }
            }
        } else if (pageSelector.value == 'Map') {
            if (output !== undefined && adventureModule !== undefined) {
                const imageDropdown = document.createElement('select') as HTMLSelectElement;
                imageDropdown.style.display = 'block';
                imageDropdown.className = 'select';

                for (const [index, image] of parsedPdf.imageDetails.entries()) {
                    if (!IsPotentialImage(image)) continue;

                    let imageElem = document.createElement('option');
                    imageElem.value = `${index}`;
                    imageElem.innerText = `page: ${image.page}, width: ${image.width}, height: ${image.height}`;
                    imageDropdown.appendChild(imageElem);
                }

                const outputTextArea = document.createElement('textarea') as HTMLTextAreaElement;
                outputTextArea.rows = 10;
                outputTextArea.cols = 80;

                const mapCanvas = document.createElement('canvas') as HTMLCanvasElement;

                const horizContainer = document.createElement('div');
                const inputLeft = document.createElement('input') as HTMLInputElement;
                inputLeft.type = 'number';
                inputLeft.value = '0';
                inputLeft.style.width = '100px';
                const inputRight = document.createElement('input') as HTMLInputElement;
                inputRight.type = 'number';
                inputRight.value = '0';
                inputRight.style.width = '100px';
                const inputCols = document.createElement('input') as HTMLInputElement;
                inputCols.type = 'number';
                inputCols.value = '10';
                inputCols.style.width = '100px';
                horizContainer.appendChild(inputLeft);
                horizContainer.appendChild(inputRight);
                horizContainer.appendChild(inputCols);

                const vertContainer = document.createElement('div');
                const inputTop = document.createElement('input') as HTMLInputElement;
                inputTop.type = 'number';
                inputTop.value = '0';
                inputTop.style.width = '100px';
                const inputBottom = document.createElement('input') as HTMLInputElement;
                inputBottom.type = 'number';
                inputBottom.value = '0';
                inputBottom.style.width = '100px';
                const inputRows = document.createElement('input') as HTMLInputElement;
                inputRows.type = 'number';
                inputRows.value = '10';
                inputRows.style.width = '100px';
                vertContainer.appendChild(inputTop);
                vertContainer.appendChild(inputBottom);
                vertContainer.appendChild(inputRows);

                pageContainer.appendChild(outputTextArea);
                pageContainer.appendChild(imageDropdown);
                pageContainer.appendChild(horizContainer);
                pageContainer.appendChild(vertContainer);
                pageContainer.appendChild(mapCanvas);

                const updateMap = async () => {
                    const image = parsedPdf.imageDetails[parseInt(imageDropdown.value, 10)];
                    const left = parseInt(inputLeft.value, 10);
                    const right = parseInt(inputRight.value, 10);
                    const cols = parseInt(inputCols.value, 10);
                    const top = parseInt(inputTop.value, 10);
                    const bottom = parseInt(inputBottom.value, 10);
                    const rows = parseInt(inputRows.value, 10);

                    outputTextArea.value = `
        new ModuleScene({
            name: 'map1',
            nameInfo: { x: 16, y: 758.8362 },
            nameFn: name => sentenceCase(name),
            page: ${image.page}, width: ${image.width}, height: ${image.height},
            imageLeft: ${left}, imageRight: ${right}, imageCols: ${cols},
            imageTop: ${top}, imageBottom: ${bottom}, imageRows: ${rows},
            globalLight: true,
        }),`;

                    mapCanvas.width = image.width;
                    mapCanvas.height = image.height;

                    let ctx = mapCanvas.getContext('2d');
                    ctx.clearRect(0, 0, image.width, image.height);

                    const srcCanvas = await image.getImageCanvas();
                    ctx.drawImage(srcCanvas, 0, 0, image.width, image.height, 0, 0, image.width, image.height);

                    ctx.strokeStyle = 'red';
                    ctx.beginPath();
                    ctx.moveTo(left, 0);
                    ctx.lineTo(left, image.height);
                    ctx.moveTo(right, 0);
                    ctx.lineTo(right, image.height);
                    ctx.moveTo(0, top);
                    ctx.lineTo(image.width, top);
                    ctx.moveTo(0, bottom);
                    ctx.lineTo(image.width, bottom);
                    ctx.stroke();

                    ctx.strokeStyle = 'gray';
                    ctx.beginPath();
                    for (let i = 1; i < cols; i++) {
                        const x = left + (right - left) * i / cols;
                        ctx.moveTo(x, 0);
                        ctx.lineTo(x, image.height);
                    }
                    for (let i = 1; i < rows; i++) {
                        const y = top + (bottom - top) * i / rows;
                        ctx.moveTo(0, y);
                        ctx.lineTo(image.width, y);
                    }
                    ctx.stroke();

                    // ctx.fillStyle = '#000';
                    // ctx.fillRect(0, 0, image.width, image.height);
                };

                imageDropdown.onchange = (ev: Event) => {
                    const image = parsedPdf.imageDetails[parseInt(imageDropdown.value, 10)];
                    inputLeft.value = '0';
                    inputRight.value = `${image.width}`;
                    inputTop.value = '0';
                    inputBottom.value = `${image.height}`;
                    updateMap();
                };
                inputLeft.onchange = (ev: Event) => { updateMap(); };
                inputRight.onchange = (ev: Event) => { updateMap(); };
                inputCols.onchange = (ev: Event) => { updateMap(); };
                inputTop.onchange = (ev: Event) => { updateMap(); };
                inputBottom.onchange = (ev: Event) => { updateMap(); };
                inputRows.onchange = (ev: Event) => { updateMap(); };
                updateMap();
            }
        } else if (pageSelector.value == 'Images') {
            let imageNum = 0;
            for (let image of parsedPdf.imageDetails) {
                if (!IsPotentialImage(image)) continue;
    
                let imageDebug = document.createElement("div");
    
                let desc = document.createElement("input")
                desc.style.display = 'block';
                desc.type = 'text';
                desc.style.width = '500px';
                desc.onclick = (ev: MouseEvent) => { desc.focus(); desc.select(); };
                desc.value = `page: ${image.page}, width: ${image.width}, height: ${image.height}`;
                imageDebug.appendChild(desc);
    
                let canvas = await image.getImageCanvas();
                canvas.style.cssText = '';
                //canvas.style.filter = 'blur(20px)';
                imageDebug.appendChild(canvas);
    
                pageContainer.appendChild(imageDebug);
            }        
        } else {
            const pageNum = parseInt(pageSelector.value, 10);
            for (const item of parsedPdf.textDetails.filter(x => x.page == pageNum)) {
                const p = document.createElement('div');
                //p.innerText = `${item.page} - (${item.x}, ${item.y}) - ${item.fontId} ${item.isLeftAligned ? '|' : item.isIndented ? '->' : ''} - ${item.text}`;
                p.innerText = item.text;
                p.style.position = 'absolute';
                p.style.left = item.x + 'px';
                p.style.bottom = item.y + 'px';
                p.style.fontSize = item.fontSize + 'px';
                if (item.isLeftAligned)
                    p.style.borderLeft = '1px solid red';
                if (item.isIndented)
                    p.style.borderLeft = '1px solid blue';
                p.title = `${item.page} - (${item.x}, ${item.y}) - ${item.fontId}/${item.color} ${item.isLeftAligned ? '|' : item.isIndented ? '->' : ''} - ${item.text}`;
                p.onclick = (e: MouseEvent) => {
                    let text : string;
                    if (e.shiftKey && e.ctrlKey) {
                        text = `nameInfo: { x: ${item.x}, y: ${item.y} },`;
                    } else if (e.shiftKey) {
                        text = `Core.markIndented(text, ${item.page}, ${item.x}, ${item.y});`;
                    } else if (e.ctrlKey) {
                        text = `Core.mergeWithNext(pdf.textDetails, ${item.page}, ${item.x}, ${item.y});`;
                    } else {
                        text = `Core.markStartOfLine(text, ${item.page}, ${item.x}, ${item.y});`;
                    }
                    copyToClipboard(text);
                };
                //p.onclick = (_) => copyToClipboard(`${item.page} - (${item.x}, ${item.y}) - ${item.fontId} ${item.isLeftAligned ? '|' : item.isIndented ? '->' : ''} - ${item.text}`);
                pageContainer.appendChild(p);
            }
            pageContainer.style.height = '900px';
        }
    }

    pageSelector.style.display = 'block';
    pageSelector.onchange = (event: Event) => { updateDebug(); };
    for (const pageNum of [...new Set(parsedPdf.textDetails.map(t => t.page))]) {
        let option = document.createElement('option');
        option.text = ''+pageNum;
        pageSelector.appendChild(option);
    }

    const downloadRawJson = document.getElementById('downloadRawJson') as HTMLButtonElement;
    downloadRawJson.disabled = false;
    downloadRawJson.onclick = (e: any) => {
        var blob = new Blob([JSON.stringify(parsedPdf.textDetails)], {type: "application/json"});
        FileSaver.saveAs(blob, 'data.json');
    };

    var adventureModule : ModuleDescription;
    if (forceAdventure !== undefined) {
        adventureModule = adventures[forceAdventure];
    } else {
        adventureModule = adventures.find(a => a.detect(parsedPdf));
    }

    if (adventureModule === undefined) {
        progressFn('failed to detect pdf. cannot proceed :(', 0);
        return;
    }

    var output = new WebModuleOutput(adventureModule.fullName);

    let importer = new ModuleImporter();
    const success = await importer.import(adventureModule, parsedPdf, output);
    if (!success) return;

    //pageSelector.value = 'Map';
    updateDebug();

    output.progressFn('success. download now!', 1);

    const downloadButton = document.getElementById('downloadButton') as HTMLButtonElement;
    downloadButton.disabled = false;
    downloadButton.onclick = () => {
        output.finalize();

        zip.generateAsync({type:'blob'})
        .then(blob => FileSaver.saveAs(blob, `${adventureModule.prefix}.zip`));
    };
}


if (pdfPath !== undefined) {
    generateModule(pdfPath);
}

window.onload = (_:any) => {
    const dropArea = document.getElementById('dropLocation');

    ['dragenter', 'dragover', 'dragleave', 'drop'].forEach(eventName => {
        dropArea.addEventListener(eventName, (e: any) => {
            e.preventDefault()
            e.stopPropagation()
        }, false);
    });

    ['dragenter', 'dragover'].forEach(eventName => {
        dropArea.addEventListener(eventName, e => dropArea.classList.add('has-background-success'), false);
    });

    ['dragleave', 'drop'].forEach(eventName => {
        dropArea.addEventListener(eventName, e => dropArea.classList.remove('has-background-success'), false);
    });


    dropArea.addEventListener('drop', handleDrop, false);
    function handleDrop(e: any) {
        e.preventDefault();
        const file = e.dataTransfer.files[0];
        const url = URL.createObjectURL(file);
        generateModule(url);
    }
};
