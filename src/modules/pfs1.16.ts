import { ModuleDescription, ModuleData, AdventureImage, CaptionImage } from "./IAdventure";
import { ModuleScene, ModuleSceneTiled } from "./ModuleScene";
import { PdfData, PdfImage } from "../parse";
import * as PFS from "./pfs";

const adventureModule : ModuleDescription = {
    prefix: "pfs-1-16",
    fullName: "PFS #1-16 - The Opal of Bhopan",
    detect: (pdf: PdfData) => {
        return pdf.textDetails.some(t => t.text == 'PATHFINDER SOCIETY SCENARIO #1–16' && t.fontId == 'GoodOT-CondBold:10' && t.page == 1);
    },
    extractDetails: PFS.extractDetails,
    pages: [
        new PFS.TwoColumnWithRightCutout(3, 330),
        new PFS.TwoColumn(4),
        new PFS.TwoColumn(5),
        new PFS.TwoColumn(6),
        // 7 is map
        new PFS.TwoColumn(8),
        new PFS.TwoColumn(9),
        new PFS.TwoColumnWithLeftCutout(10, 412),
        new PFS.TwoColumn(11),
        new PFS.TwoColumn(12),
        new PFS.TwoColumn(13),
        new PFS.TwoColumnWithLeftCutout(14, 185),
        new PFS.TwoColumn(15),
        new PFS.TwoColumn(16),
        new PFS.TwoColumn(17),
        new PFS.TwoColumn(18),
        // 19 is map
        new PFS.TwoColumn(20),
        new PFS.TwoColumn(21),

        // appendix
        new PFS.TwoColumnWithRightCutout(22, 490, 'appendix1a'),
        new PFS.TwoColumn(23, 'appendix1a'),
        new PFS.TwoColumnWithRightCutout(24, 500, 'appendix1b'),
        new PFS.TwoColumnWithRightCutout(25, 535, 'appendix1c'),
        new PFS.TwoColumn(26, 'appendix1c'),
        new PFS.TwoColumnWithRightCutout(27, 500, 'appendix1d'),
        
        new PFS.TwoColumnWithRightCutout(28, 520, 'appendix2a'),
        new PFS.TwoColumn(29, 'appendix2a'),
        new PFS.TwoColumnWithRightCutout(30, 500, 'appendix2b'),
        new PFS.TwoColumnWithRightCutout(31, 535, 'appendix2c'),
        new PFS.TwoColumn(32, 'appendix2c'),
        new PFS.TwoColumnWithRightCutout(33, 500, 'appendix2d'),
    ],

    scenes: [
        new ModuleScene({
            name: 'map1',
            nameInfo: { x: 72, y: 674.5 },
            nameFn: name => name.substring(3),
            page: 7, width: 1305, height: 1048,
            imageLeft: 10, imageTop: 11, imageRight: 1294, imageBottom: 1038,
            imageRows: 30, imageCols: 24,
            rotateCW: 3,
        }),
        new ModuleSceneTiled({
            name: 'map2',
            nameInfo: { x: 95.7348, y: 677.2938 },
            nameFn: name => name.substring(3),
            page: 19,
            imageFn: (img: PdfImage) => {
                if (img.page != 19) return undefined;

                if (img.width == 298 && img.height == 305)
                    return { imageLeft: 8, imageTop: 3, imageRight: 261, imageBottom: 293 };
                if (img.width == 456 && img.height == 458)
                    return { imageLeft: 11, imageTop: 12, imageRight: 443, imageBottom: 444 };
                if (img.width == 319 && img.height == 320)
                    return { imageLeft: 8, imageTop: 8, imageRight: 311, imageBottom: 311 };
    
                return undefined;
            },
            imageRows: 6, imageCols: 6,
            numTileCols: 3, numTileRows: 4,
            fillColor: '#000',
            tilePositions: [
                [0, 0],
                [1, 0],
                [2, 0],
                [0, 1],
                [0, 2],
                [0, 3],
                [1, 1],
                [1, 2],
                [1, 3],
                [2, 3],
                [2, 1],
            ],
            tileRotations: [
                [2, 3, 1],
                [3, 3, 1],
                [1, 1, 2],
                [2, 1, 3],
            ]
        }),
        new ModuleSceneTiled({
            name: 'map3',
            nameInfo: { x: 72, y: 375 },
            nameFn: name => name.substring(3),
            imageFn: (img: PdfImage) => {
                if (img.page != 20) return undefined;

                if (img.width == 325 && img.height == 325)
                    return { imageLeft: 8, imageTop: 8, imageRight: 317, imageBottom: 317 };
                if (img.width == 460 && img.height == 461)
                    return { imageLeft: 14, imageTop: 8, imageRight: 446, imageBottom: 446 };
    
                return undefined;
            },
            page: 20,
            imageRows: 6, imageCols: 6,
            numTileCols: 3, numTileRows: 2,
            fillColor: '#000',
            tilePositions: [
                [0, 0],
                [0, 1],
                [1, 0],
                [1, 1],
                [2, 0],
                [2, 1],
            ],
            tileRotations: [
                [3, 0, 0],
                [2, 2, 1],
            ]
        }),

    ],
    journals: [
        new CaptionImage({ name: 'journal1', page: 35, width: 1253, height: 1288 }),
        new CaptionImage({ name: 'npc1', page: 11, width: 753, height: 1131 }),
        new CaptionImage({ name: 'npc2', page: 13, width: 828, height: 1190 }),
        new CaptionImage({ name: 'npc3', page: 34, width: 1251, height: 1071 }),
    ],
    finalize: (moduleData: ModuleData) => {
    }
};

export default adventureModule;