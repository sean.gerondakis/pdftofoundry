import { ModuleDescription, ModuleData, CaptionImage } from "./IAdventure";
import { ModuleScene } from "./ModuleScene";
import { PdfData } from "../parse";
import * as PFS from "./pfs";

const adventureModule : ModuleDescription = {
    prefix: "pfs-q3",
    fullName: "PFS Q3 - Grehunde's Gorget",
    detect: (pdf: PdfData) => {
        return pdf.textDetails.some(t => t.text == 'PATHFINDER SOCIETY QUEST #3' && t.fontId == 'GoodOT-CondBold:10' && t.page == 1);
    },
    extractDetails: PFS.extractDetails,
    pages: [
        new PFS.TwoColumn(3),
        new PFS.TwoColumn(4),
        // 5 is map
        new PFS.TwoColumn(6),
        new PFS.TwoColumn(7),
    ],
    scenes: [
        new ModuleScene({
            name: 'map1',
            page: 5, width: 1326, height: 1065,
            rotateCW: 3,
            imageLeft: 11, imageTop: 11, imageRight: 1315, imageBottom: 1054,
            imageRows: 30, imageCols: 24
        }),
    ],
    journals: [
        new CaptionImage({ name: 'journal1', page: 6, width: 496, height: 566 }),
        new CaptionImage({ name: 'npc1', page: 8, width: 1091, height: 1636 }),
        new CaptionImage({ name: 'npc2', page: 9, width: 1258, height: 1258 }),
    ],
    finalize: (moduleData: ModuleData) => {
    }
};

export default adventureModule;