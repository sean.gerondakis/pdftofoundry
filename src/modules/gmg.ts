import { ModuleDescription, ModuleData } from "./IAdventure";
import { PdfData } from "../parse";
import * as Core from "./core";

const gmgModule : ModuleDescription = {
    prefix: "gmg",
    fullName: "Pathfinder 2E Gamemastery Guide",
    detect: (pdf: PdfData) => {
        return pdf.textDetails.some(t => t.text == 'GAMEMASTERY GUIDE' && t.fontId == 'RomicStd-Bold:53.3514');
    },
    extractDetails: Core.extractDetails,
    pages: [
        // hazards
        new Core.LeftTwoColumn(75),
        new Core.RightTwoColumn(76),
        new Core.LeftTwoColumn(77),
        new Core.RightTwoColumn(78),
        new Core.LeftTwoColumn(79),
        new Core.RightTwoColumn(80),
        new Core.LeftTwoColumn(81),
        new Core.RightTwoColumn(82),
    ],
    scenes: [
    ],
    journals: [
    ],
    finalize: () => {
    }
};

export default gmgModule;