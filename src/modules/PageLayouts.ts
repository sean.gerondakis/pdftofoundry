import { PdfData, TextDetail } from "../parse";

export interface PageLayoutEmitFunction {
    (section: string, text: TextDetail): void;
}

export class PageLayout {
    pageNum: number;

    constructor(pageNum: number) {
        this.pageNum = pageNum;
    }

    getAllTextInBoundingBox(pdf: PdfData, left: number, right: number, top: number, bottom: number, emit: PageLayoutEmitFunction, section?: string) {
        let ret = pdf.textDetails
            .filter(t => t.page == this.pageNum && t.x >= left-1 && t.x < right && t.y > bottom && t.y <= top)
    
        ret.sort((l, r) => {
            if (l.y != r.y) { return r.y - l.y; }
            return l.x - r.x;
        });

        for (let text of ret) {
            text.isLeftAligned = Math.abs(text.x - left) < 1,
            text.isIndented = Math.abs(text.x - (left + 9)) < 1,

            emit(section ?? '', text);
        }
        return ret;
    }

    getPage(pdf: PdfData, emit: PageLayoutEmitFunction) {
        for (let text of pdf.textDetails) {
            emit('', text);
        }
    }
}
