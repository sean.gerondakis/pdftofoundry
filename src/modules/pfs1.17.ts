import { ModuleDescription, ModuleData, AdventureImage, CaptionImage } from "./IAdventure";
import { ModuleScene, ModuleSceneTiled } from "./ModuleScene";
import { PdfData, PdfImage } from "../parse";
import * as PFS from "./pfs";

const adventureModule : ModuleDescription = {
    prefix: "pfs-1-17",
    fullName: "PFS #1-17 - The Thorned Monarch",
    detect: (pdf: PdfData) => {
        return pdf.textDetails.some(t => t.text == 'PATHFINDER SOCIETY SCENARIO #1–17' && t.fontId == 'GoodOT-CondBold:10' && t.page == 1);
    },
    extractDetails: PFS.extractDetails,
    pages: [
        new PFS.TwoColumnWithRightCutout(3, 330),
        new PFS.TwoColumn(4),
        new PFS.TwoColumnWithLeftCutout(5, 390),
        new PFS.TwoColumn(6),
        // 7 is map
        new PFS.TwoColumn(8),
        // 9 is map
        new PFS.TwoColumnWithRightCutout(10, 600),
        new PFS.TwoColumn(11),
        // 12 is map
        new PFS.TwoColumn(13),
        new PFS.TwoColumn(14),

        // appendix
        new PFS.TwoColumnWithRightCutout(15, 580, 'appendix1a'),
        new PFS.TwoColumnWithRightCutout(16, 580, 'appendix1b'),
        new PFS.TwoColumnWithRightCutout(17, 580, 'appendix1c'),
        new PFS.TwoColumn(18, 'appendix1c'),

        new PFS.TwoColumnWithRightCutout(19, 580, 'appendix2a'),
        new PFS.TwoColumnWithRightCutout(20, 500, 'appendix2a'),
        new PFS.TwoColumnWithRightCutout(21, 580, 'appendix2c'),
        new PFS.TwoColumn(22, 'appendix2c'),
    ],

    scenes: [
        new ModuleSceneTiled({
            name: 'map1',
            nameInfo: { x: 102.2126, y: 675.2747 },
            nameFn: name => name.substring(3),
            page: 7, width: 438, height: 439,
            imageLeft: 0, imageTop: 0, imageRight: 438, imageBottom: 438,
            imageRows: 6, imageCols: 6,
            numTileCols: 2, numTileRows: 3,
            fillColor: '#000',
            tilePositions: [
                [0, 2],
                [0, 1],
                [0, 0],
                [1, 2],
                [1, 1],
                [1, 0],
            ],
            tileRotations: [
                [2, 0],
                [2, 0],
                [0, 3],
            ]
        }),
        new ModuleScene({
            name: 'map2',
            nameInfo: { x: 72, y: 674.5 },
            nameFn: name => name.substring(3),
            page: 9, width: 1305, height: 1048,
            imageLeft: 10, imageTop: 11, imageRight: 1294, imageBottom: 1038,
            imageRows: 30, imageCols: 24,
            rotateCW: 3,
        }),
        new ModuleScene({
            name: 'map3',
            nameInfo: { x: 72, y: 674.5 },
            nameFn: name => name.substring(3),
            page: 12, width: 1051, height: 1303,
            imageLeft: 20, imageTop: 22, imageRight: 1282, imageBottom: 988,
            imageRows: 23, imageCols: 30,
        }),
    ],
    journals: [
        new AdventureImage({ name: 'journal1', page: 3, width: 437, height: 299 }),
        new CaptionImage({ name: 'npc1', page: 23, width: 845, height: 1253 }),
        new CaptionImage({ name: 'npc2', page: 24, width: 1255, height: 1242 }),
        new CaptionImage({ name: 'npc3', page: 25, width: 1241, height: 1130 }),
        new CaptionImage({ name: 'npc4', page: 26, width: 1001, height: 1329 }),
    ],
    finalize: (moduleData: ModuleData) => {
    }
};

export default adventureModule;