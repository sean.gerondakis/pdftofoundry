import { ModuleDescription, ModuleData, CaptionImage } from "./IAdventure";
import { ModuleScene, ModuleSceneTiled } from "./ModuleScene";
import { PdfData } from "../parse";
import * as PFS from "./pfs";

const adventureModule : ModuleDescription = {
    prefix: "pfs-q10",
    fullName: "PFS Q10 - The Broken Scales",
    detect: (pdf: PdfData) => {
        return pdf.textDetails.some(t => t.text == 'PATHFINDER SOCIETY QUEST #10' && t.fontId == 'GoodOT-CondBold:10' && t.page == 1);
    },
    extractDetails: PFS.extractDetails,
    pages: [
        new PFS.TwoColumnWithRightCutout(3, 400),
        new PFS.TwoColumn(4),
        // 5 is map
        new PFS.TwoColumn(6),
        new PFS.TwoColumn(7),
        
        new PFS.TwoColumnWithRightCutout(8, 580, 'appendix1'),
        new PFS.TwoColumnWithRightCutout(9, 580, 'appendix2'),
    ],
    scenes: [
        new ModuleSceneTiled({
            name: 'map1',
            nameInfo: { x: 43.5, y: 675.2669 },
            nameFn: name => name.substring(3).trim(),
            page: 5, width: 431, height: 431,
            imageLeft: 0, imageTop: 0, imageRight: 431, imageBottom: 431,
            imageRows: 6, imageCols: 6,
            numTileCols: 3, numTileRows: 3,
            tileRotations: [
                [0, 1, 1],
                [1, 1, 2],
                [1, 1, 0],
            ],
            walls: [
                {"_id":"Pmv354LzuTKuKVaa","flags":{},"c":[1100,600,1100,700],"move":1,"sense":1,"door":0,"ds":0},
                {"_id":"Ospcr6ZZ8emAyWDp","flags":{},"c":[1100,700,900,700],"move":1,"sense":1,"door":0,"ds":0},
                {"_id":"tu5p0TD1VO6E70hE","flags":{},"c":[900,700,900,600],"move":1,"sense":1,"door":0,"ds":0},
                {"_id":"fRbmlPGwquDpWHwo","flags":{},"c":[900,600,700,600],"move":1,"sense":1,"door":0,"ds":0},
                {"_id":"MgldK9s7KLVlrsi4","flags":{},"c":[700,600,700,700],"move":1,"sense":1,"door":0,"ds":0},
                {"_id":"NkjFTNPxrIFBdS6j","flags":{},"c":[700,700,500,700],"move":1,"sense":1,"door":0,"ds":0},
                {"_id":"JVqLORqTS54jnh8g","flags":{},"c":[500,700,500,1000],"move":1,"sense":1,"door":0,"ds":0},
                {"_id":"lr343mArpFnWJvfj","flags":{},"c":[500,1000,1800,1000],"move":1,"sense":1,"door":0,"ds":0},
                {"_id":"wBSLpWmTVcXVhgga","flags":{},"c":[1800,1000,1800,1200],"move":1,"sense":1,"door":0,"ds":0},
                {"_id":"R6yODtX9Gwv5abRp","flags":{},"c":[1800,1200,900,1200],"move":1,"sense":1,"door":0,"ds":0},
                {"_id":"pYzLPQfEcL664IA0","flags":{},"c":[900,1200,900,1100],"move":1,"sense":1,"door":0,"ds":0},
                {"_id":"pncevlv7bnepy6u8","flags":{},"c":[900,1100,600,1100],"move":1,"sense":1,"door":0,"ds":0},
                {"_id":"z07xNZ6OI2j8fBAV","flags":{},"c":[600,1100,600,1900],"move":1,"sense":1,"door":0,"ds":0},
                {"_id":"2anaT09FwPrWseDq","flags":{},"c":[600,1900,500,1900],"move":1,"sense":1,"door":0,"ds":0},
                {"_id":"93SLeXQcNunMMLsl","flags":{},"c":[500,1900,500,2200],"move":1,"sense":1,"door":0,"ds":0},
                {"_id":"tq0a7ZUhhcNB3Xqp","flags":{},"c":[500,2200,600,2200],"move":1,"sense":1,"door":0,"ds":0},
                {"_id":"NoxYHDA5sUP7aZ0c","flags":{},"c":[600,2200,600,2300],"move":1,"sense":1,"door":0,"ds":0},
                {"_id":"pQ38Jl06NNPSWw7X","flags":{},"c":[600,2300,900,2300],"move":1,"sense":1,"door":0,"ds":0},
                {"_id":"8RbEwuOCSCEpKJGJ","flags":{},"c":[900,2300,900,2200],"move":1,"sense":1,"door":0,"ds":0},
                {"_id":"NKzSS7HDyas9Gg1y","flags":{},"c":[900,2200,1800,2200],"move":1,"sense":1,"door":0,"ds":0},
                {"_id":"scMZUkoWFuWQDNoy","flags":{},"c":[1800,2200,1800,2300],"move":1,"sense":1,"door":0,"ds":0},
                {"_id":"GXiRDWGAWJe1OxpX","flags":{},"c":[1800,2300,2300,2300],"move":1,"sense":1,"door":0,"ds":0},
                {"_id":"eL4j8yRG7IOiBxuZ","flags":{},"c":[2300,2300,2300,1700],"move":1,"sense":1,"door":0,"ds":0},
                {"_id":"OCUk3x6VGKdrhqBt","flags":{},"c":[2300,1700,1800,1700],"move":1,"sense":1,"door":0,"ds":0},
                {"_id":"t3RqkrwE4ibCaM7j","flags":{},"c":[1800,1700,1800,1800],"move":1,"sense":1,"door":0,"ds":0},
                {"_id":"W0P2Scw79T1VXAQV","flags":{},"c":[1800,1800,900,1800],"move":1,"sense":1,"door":0,"ds":0},
                {"_id":"LPofUfE5SMWz4rg2","flags":{},"c":[900,1800,900,1600],"move":1,"sense":1,"door":0,"ds":0},
                {"_id":"YQQbN30AzMaRtYnn","flags":{},"c":[900,1600,2200,1600],"move":1,"sense":1,"door":0,"ds":0},
                {"_id":"Z7aJjySk0YJcnzn8","flags":{},"c":[2200,1600,2200,600],"move":1,"sense":1,"door":0,"ds":0},
                {"_id":"3QXus3IesG1LMKWV","flags":{},"c":[2200,600,1100,600],"move":1,"sense":1,"door":0,"ds":0}
            ],
            journals: [
                {"x":850,"y":650,"name":"A1."},
                {"x":1550,"y":850,"name":"A2."},
                {"x":1950,"y":750,"name":"A3."},
                {"x":2050,"y":1450,"name":"A4."},
                {"x":1450,"y":1350,"name":"A5."},
                {"x":850,"y":1650,"name":"A6."},
                {"x":750,"y":1950,"name":"A7."},
                {"x":1250,"y":1850,"name":"A8."},
                {"x":2150,"y":1950,"name":"A9."}
            ],
        }),
    ],
    journals: [
        new CaptionImage({ name: 'journal1', page: 3, width: 409, height: 278 }),
        new CaptionImage({ name: 'npc1', page: 10, width: 1216, height: 1236 }),
        new CaptionImage({ name: 'npc2', page: 11, width: 1261, height: 1174 }),
    ],
    finalize: (moduleData: ModuleData) => {
    }
};

export default adventureModule;