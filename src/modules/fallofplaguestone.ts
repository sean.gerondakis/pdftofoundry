import { ModuleDescription, ModuleData, AdventureImage, CaptionImage } from "./IAdventure";
import { ModuleScene } from "./ModuleScene";
import { PdfData, TextDetail } from "../parse";
import { PageLayoutEmitFunction, PageLayout } from "./PageLayouts";
import { Journal } from "../foundry";
import { ParserText, makeContiguousParser } from "./parser";
import { parseMonsterOrHazard } from "./core";

export class ChapterHeadingPageLayout extends PageLayout {
    constructor(pageNum: number) {
        super(pageNum);
    }
    getPage(pdf: PdfData, emit: PageLayoutEmitFunction) {
        const headerBlocks = pdf.textDetails
            .filter(t => t.page == this.pageNum && t.fontId == 'CaslonAntique:30')
            .map(t => t.y);
        const headingStartY = Math.min(...headerBlocks) + 40;

        this.getAllTextInBoundingBox(pdf, 0, 1000, 1000, headingStartY, emit);
        this.getAllTextInBoundingBox(pdf, 111, 320, headingStartY, 40, emit);
        this.getAllTextInBoundingBox(pdf, 332.25, 1000, headingStartY, 40, emit);
    }
};

export class LeftTwoColumn extends PageLayout {
    constructor(pageNum: number) {
        super(pageNum);
    }
    getPage(pdf: PdfData, emit: PageLayoutEmitFunction) {
        this.getAllTextInBoundingBox(pdf, 111, 320, 1000, 40, emit);
        this.getAllTextInBoundingBox(pdf, 332.25, 1000, 1000, 40, emit);
    }
};

export class RightTwoColumn extends PageLayout {
    constructor(pageNum: number) {
        super(pageNum);
    }
    getPage(pdf: PdfData, emit: PageLayoutEmitFunction) {
        this.getAllTextInBoundingBox(pdf, 67.5, 280, 1000, 40, emit);
        this.getAllTextInBoundingBox(pdf, 288.75, 520, 1000, 40, emit);
    }
};

export class LeftTwoColumnWithLeftCutout extends PageLayout {
    cutoutPos: number;
    section?: string;

    constructor(pageNum: number, cutoutPos: number, section?: string) {
        super(pageNum);
        this.cutoutPos = cutoutPos;
        this.section = section;
    }
    getPage(pdf: PdfData, emit: PageLayoutEmitFunction) {
        this.getAllTextInBoundingBox(pdf, 111, 320, this.cutoutPos, 40, emit, this.section);
        this.getAllTextInBoundingBox(pdf, 332.25, 1000, 1000, 40, emit, this.section);
        this.getAllTextInBoundingBox(pdf, 111, 320, 700, this.cutoutPos, emit, 'sidebar'+this.pageNum);
    }
};

export class RightTwoColumnWithRightCutout extends PageLayout {
    cutoutPos: number;
    section?: string;

    constructor(pageNum: number, cutoutPos: number, section?: string) {
        super(pageNum);
        this.cutoutPos = cutoutPos;
        this.section = section;
    }
    getPage(pdf: PdfData, emit: PageLayoutEmitFunction) {
        this.getAllTextInBoundingBox(pdf, 67.5, 280, 700, 40, emit, this.section);
        this.getAllTextInBoundingBox(pdf, 288.75, 520, this.cutoutPos, 40, emit, this.section);
        this.getAllTextInBoundingBox(pdf, 288.75, 520, 700, this.cutoutPos, emit, 'sidebar'+this.pageNum);
    }
};

export class LeftTwoColumnWithMap extends PageLayout {
    cutoutPos?: number;

    constructor(pageNum: number, cutoutPos?: number) {
        super(pageNum);
        this.cutoutPos = cutoutPos ?? 327;
    }
    getPage(pdf: PdfData, emit: PageLayoutEmitFunction) {
        this.getAllTextInBoundingBox(pdf, 111, 320, this.cutoutPos, 40, emit);
        this.getAllTextInBoundingBox(pdf, 332.25, 1000, this.cutoutPos, 40, emit);
    }
};

const parseIcon = (p: ParserText) => {
    return p.consume().text;
};

const parseItalic = makeContiguousParser("<i>", "</i>");
const parseBold = makeContiguousParser("<b>", "</b>");
const parseHeading = makeContiguousParser("<h3>", "</h3>");

const headingFontIds = ['GoodOT-News:12', 'GoodOT:12'];
const normalFontIds = ['SabonLTStd-Roman:9', 'Sabon-Roman:9'];
const italicFontIds = ['SabonLTStd-Italic:9'];
const boldFontIds = ['TimesNewRomanPS-BoldMT:9', 'TimesNewRomanPSMT:9'];

const paragraphFontIds = headingFontIds
    .concat(normalFontIds)
    .concat(italicFontIds)
    .concat(boldFontIds);

const parseParagraph = (p: ParserText) => {
    let current = '';

    while (p.anyRemaining()) {
        const peekFont = p.peek().fontId;
        if (normalFontIds.includes(peekFont)) {
            current += p.consume().text;
        } else if (headingFontIds.includes(peekFont)) {
            current += parseHeading(p);
        } else if (italicFontIds.includes(peekFont)) {
            current += parseItalic(p);
        } else if (boldFontIds.includes(peekFont)) {
            current += parseBold(p);
        } else {
            break;
        }

        if (p.anyRemaining() && p.peek().isIndented) {
            break;
        }
    }
    return "<p>" + current + "</p>";
};

const parseBlockquote = (p: ParserText) => {
    let current = '';
    while (p.anyRemaining()) {
        if (p.peek().fontId == 'GoodOT:9') {
            current += p.consume().text;
        } else if (p.peek().fontId == 'GoodOT-Italic:9') {
            current += parseItalic(p);
        } else if (p.peek().fontId == 'GoodOT-Bold:9') {
            current += parseBold(p);
        } else {
            break;
        }
    }
    return "<blockquote>" + current + "</blockquote>";
};

const parseAside = (p: ParserText) => {
    return new Journal(p.consume().text, parseBlockquote(p));
};

const sectionHeadingFonts = ['CaslonAntique:30', 'CaslonAntique:14', 'GoodOT-Bold:12'];

const parseSectionBody = (p: ParserText, moduleData: ModuleData) => {
    let current = '';

    while (p.anyRemaining()) {
        let cur = p.peek();
        if (cur.fontId == 'SabonLTStd-Roman:9' || p.peek().fontId == 'SabonLTStd-Italic:9' || p.peek().fontId == 'TimesNewRomanPS-BoldMT:9') {
            current += parseParagraph(p);
        } else if (cur.fontId == 'GoodOT:9' || p.peek().fontId == 'GoodOT-Bold:9') {
            current += parseBlockquote(p);
        } else if (cur.fontId == 'GoodOT-CondBold:12') {
            current += parseMonsterOrHazard(p, moduleData);
        } else if (cur.fontId == 'GoodOT-Bold:11') {
            moduleData.journalEntries.push(parseAside(p));
        } else if (cur.fontId == 'GoodOT-Bold:12' && !(cur.text.match(/^[ABCDEFGHIJKLMNOPQRSTUVWXYZ]\d+\./) || cur.text.match(/^Event \d+:/))) {
            current += parseHeading(p);
        } else if (cur.fontId == 'CaslonAntique:42.9367') {
            current = p.consume().text + current;
        } else {
            //console.log(`abandoning section on: ${cur.fontId} :: (${cur.x}, ${cur.y}) :: ${cur.text}`);
            break;
        }
    }

    return current;
};

const parseSection = (p: ParserText, moduleData: ModuleData) => {
    let name = p.consume().text;
    if (p.peek().fontId == 'GoodOT-Bold:14') {
        name += "(" + p.consume().text + ")";
    }

    while (sectionHeadingFonts.includes(p.peek().fontId)) {
        name += p.consume().text;
    }

    const current = parseSectionBody(p, moduleData);

    moduleData.journalEntries.push(new Journal(name, current));
};


function extractDetails(text: Array<TextDetail>, moduleData: ModuleData) {
    const blacklistedFonts = ['Times-Italic', 'CaslonAntique:12']
    text = text.filter(t => !blacklistedFonts.includes(t.fontId));

    let p = new ParserText(text);

    while (p.anyRemaining()) {
        let cur = p.peek();
        if (sectionHeadingFonts.includes(cur.fontId)) {
            //console.log("starting section: " + cur.text);
            parseSection(p, moduleData);
        } else if (cur.fontId == 'GoodOT-Bold:11') {
            moduleData.journalEntries.push(parseAside(p));
        } else {
            let item = p.consume();
            console.log(`unknown ${item.fontId} :: ${item.page} (${item.x}, ${item.y}) :: ${item.text}`);
        }
    }
}



const adventureModule : ModuleDescription = {
    prefix: "fall-of-plaguestone",
    fullName: "The Fall of Plaguestone",
    detect: (pdf: PdfData) => {
        return pdf.textDetails.some(t => t.text == 'THE FALL OF ' && t.fontId == 'CaslonAntique:30');
    },
    extractDetails: extractDetails,
    pages: [
        // chapter 1
        new ChapterHeadingPageLayout(6),
        new RightTwoColumnWithRightCutout(7, 450),
        // maps on 8
        new RightTwoColumn(9),
        new LeftTwoColumnWithLeftCutout(10, 440),
        new RightTwoColumn(11),
        new LeftTwoColumn(12),
        new RightTwoColumn(13),
        new LeftTwoColumn(14),
        new RightTwoColumn(15),
        new LeftTwoColumn(16),
        new RightTwoColumn(17),
        // map on 18
        new RightTwoColumn(19),
        new LeftTwoColumn(20),
        new RightTwoColumn(21),
        new LeftTwoColumn(22),
        new RightTwoColumn(23),
        new LeftTwoColumnWithMap(24, 460),
        new RightTwoColumn(25),


        // chapter 2
        new ChapterHeadingPageLayout(26),
        new RightTwoColumn(27),
        new LeftTwoColumnWithMap(28),
         new RightTwoColumn(29),
        // 30 is map
        new RightTwoColumn(31),
        new LeftTwoColumn(32),
        new RightTwoColumn(33),
        new LeftTwoColumn(34),
        new RightTwoColumn(35),
        new LeftTwoColumn(36),
        new RightTwoColumn(37),

        // chapter 3
        new ChapterHeadingPageLayout(38),
        new RightTwoColumn(39),
        new LeftTwoColumn(40),
        new RightTwoColumn(41),
        new LeftTwoColumn(42),
        new RightTwoColumn(43),
        new LeftTwoColumn(44),
        new RightTwoColumn(45),
        new LeftTwoColumn(46),
        new RightTwoColumn(47),
        new LeftTwoColumn(48),
        new RightTwoColumn(49),
        new LeftTwoColumn(50),
        new RightTwoColumn(51),

        // adventure toolbox
        new RightTwoColumn(53),
        new LeftTwoColumn(54),
        new RightTwoColumn(55),
        new LeftTwoColumnWithLeftCutout(56, 260),
        new RightTwoColumnWithRightCutout(57, 450),
        new LeftTwoColumn(58),
        new RightTwoColumn(59),
        new LeftTwoColumn(60),
        new RightTwoColumn(61),
        new LeftTwoColumn(62),
        new RightTwoColumn(63),
        new LeftTwoColumn(64),
        new RightTwoColumn(65),
        
    ],
    scenes: [
        new ModuleScene({
            name: 'map1',
            page: 2, width: 1166, height: 1555,
            imageLeft: 29, imageTop: 117, imageRight: 999, imageBottom: 774,
            imageRows: 19, imageCols: 28
        }),
        new ModuleScene({
            name: 'map2',
            page: 2, width: 1163, height: 1550,
            gridType: 'gridless',
            imageLeft: 0, imageTop: 0, imageRight: 1000, imageBottom: 1500,
            imageRows: 15, imageCols: 10
        }),
        new ModuleScene({
            name: 'map3',
            page: 8, width: 1261, height: 1636,
            cropLeft: 0, cropTop: 0, cropRight: 572, cropBottom: 741,
            imageLeft: 8, imageTop: 6, imageRight: 538, imageBottom: 726,
            imageRows: 19, imageCols: 14
        }),
        new ModuleScene({
            name: 'map4',
            page: 8, width: 1261, height: 1636,
            cropLeft: 0, cropTop: 765, cropRight: 572, cropBottom: 1267,
            imageLeft: 27, imageTop: 801, imageRight: 558, imageBottom: 1256,
            imageRows: 12, imageCols: 14
        }),
        new ModuleScene({
            name: 'map5',
            page: 8, width: 1261, height: 1636,
            cropLeft: 0, cropTop: 1293, cropRight: 572, cropBottom: 1636,
            imageLeft: 22, imageTop: 1305, imageRight: 553, imageBottom: 1609,
            imageRows: 8, imageCols: 14
        }),
        new ModuleScene({
            name: 'map6',
            page: 8, width: 1261, height: 1636,
            cropLeft: 598, cropTop: 0, cropRight: 1261, cropBottom: 740,
            imageLeft: 631, imageTop: 18, imageRight: 1238, imageBottom: 701,
            imageRows: 18, imageCols: 16
        }),
        new ModuleScene({
            name: 'map7',
            page: 8, width: 1261, height: 1636,
            cropLeft: 598, cropTop: 766, cropRight: 1261, cropBottom: 1636,
            imageLeft: 622, imageTop: 785, imageRight: 1228, imageBottom: 1619,
            imageRows: 22, imageCols: 16
        }),
        new ModuleScene({
            name: 'map8',
            page: 18, width: 1261, height: 1636,
            imageLeft: 21, imageTop: 21, imageRight: 1234, imageBottom: 1461,
            imageRows: 38, imageCols: 32
        }),
        new ModuleScene({
            name: 'map9',
            page: 28, width: 1262, height: 903,
            imageLeft: 44, imageTop: 47, imageRight: 1239, imageBottom: 875,
            imageRows: 18, imageCols: 26
        }),
        new ModuleScene({
            name: 'map10',
            page: 30, width: 1261, height: 1636,
            imageLeft: 27, imageTop: 296, imageRight: 1108, imageBottom: 1632,
            imageRows: 47, imageCols: 38
        }),
    ],
    journals: [
        new AdventureImage({ name: 'journal1', page: 1, width: 1261, height: 1636 }),
        new AdventureImage({ name: 'journal2', page: 1, width: 1261, height: 715 }),
        new AdventureImage({ name: 'journal3', page: 4, width: 1259, height: 1636 }),
        new AdventureImage({ name: 'journal4', page: 6, width: 1242, height: 740 }),
        new CaptionImage({ name: 'journal5', page: 15, width: 423, height: 441 }),
        new AdventureImage({ name: 'journal6', page: 26, width: 1246, height: 715 }),
        new AdventureImage({ name: 'journal7', page: 38, width: 1238, height: 715 }),
        new AdventureImage({ name: 'journal8', page: 52, width: 1238, height: 778 }),
        new AdventureImage({ name: 'journal9', page: 58, width: 249, height: 405 }),
        new AdventureImage({ name: 'journal10', page: 58, width: 217, height: 374 }),
        new AdventureImage({ name: 'journal11', page: 60, width: 253, height: 1320 }),
        new CaptionImage({ name: 'npc1', page: 9, width: 441, height: 441 }),
        new CaptionImage({ name: 'npc2', page: 13, width: 441, height: 441 }),
        new CaptionImage({ name: 'npc3', page: 14, width: 423, height: 441 }),
        new CaptionImage({ name: 'npc4', page: 17, width: 441, height: 441 }),
        new CaptionImage({ name: 'npc5', page: 22, width: 836, height: 1009 }),
        new CaptionImage({ name: 'npc6', page: 23, width: 563, height: 1156 }),
        new CaptionImage({ name: 'npc7', page: 27, width: 449, height: 469 }),
        new CaptionImage({ name: 'npc8', page: 32, width: 879, height: 1104 }),
        new CaptionImage({ name: 'npc9', page: 34, width: 894, height: 886 }),
        new CaptionImage({ name: 'npc10', page: 36, width: 842, height: 1089 }),
        new CaptionImage({ name: 'npc11', page: 40, width: 903, height: 1082 }),
        new CaptionImage({ name: 'npc12', page: 42, width: 844, height: 1128 }),
        new CaptionImage({ name: 'npc13', page: 43, width: 600, height: 776 }),
        new CaptionImage({ name: 'npc14', page: 46, width: 984, height: 1011 }),
        new CaptionImage({ name: 'npc15', page: 48, width: 747, height: 834 }),
        new CaptionImage({ name: 'npc16', page: 49, width: 735, height: 1040 }),
        new CaptionImage({ name: 'npc17', page: 51, width: 449, height: 469 }),
        new CaptionImage({ name: 'npc18', page: 53, width: 441, height: 441 }),
        new CaptionImage({ name: 'npc19', page: 54, width: 441, height: 441 }),
        new CaptionImage({ name: 'npc20', page: 55, width: 421, height: 441 }),
        new CaptionImage({ name: 'npc21', page: 62, width: 886, height: 1110 }),
        new CaptionImage({ name: 'npc22', page: 64, width: 740, height: 836 }),
        new CaptionImage({ name: 'npc23', page: 65, width: 718, height: 821 }),
    ],
    finalize: (moduleData: ModuleData) => {
    }
};

export default adventureModule;