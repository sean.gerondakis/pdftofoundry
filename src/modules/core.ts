import { PageLayout, PageLayoutEmitFunction } from "./PageLayouts";
import { PdfData, TextDetail } from "../parse";
import { ParserText, makeContiguousParser } from "./parser";
import { ModuleData } from "./IAdventure";
import { Journal, NPCActor, LoreItem, EquipmentItem, MeleeItem, ActionItem, HazardActor } from "../foundry";
import { FeatItem, SpellItem } from "../foundry/item";

const ancestries = ['dwarf', 'elf', 'gnome', 'goblin', 'halfelf', 'halfling', 'halforc', 'human', 'hobgoblin', 'leshy', 'lizardfolk', 'aasimar', 'catfolk', 'changeling', 'dhampir', 'duskwalker', 'geniekin', 'kobold', 'orc', 'ratfolk', 'shoony', 'tengu', 'tiefling'];
const classes = ['alchemist', 'barbarian', 'bard', 'champion', 'cleric', 'druid', 'fighter', 'investigator', 'monk', 'oracle', 'ranger', 'rogue', 'sorcerer', 'swashbuckler', 'witch', 'wizard'];

export function sentenceCase(name: string) {
    return name.split(' ')
        .map(w => w.length > 1 ? w[0].toUpperCase() + w.substr(1).toLowerCase() : w)
        .join(' ')
        .trim();
}

export function mergeWithNext(text: TextDetail[], page: number, x: number, y: number) {
    const i = text.findIndex(t => t.page == page && Math.abs(t.x - x) < 1 && Math.abs(t.y - y) < 1);
    if (i !== -1) {
        let t = text[i].text;
        text.splice(i, 1);
        text[i].text = t + text[i].text;
    }
}

export function markStartOfLine(text: TextDetail[], page: number, x: number, y: number) {
    const i = text.findIndex(t => t.page == page && Math.abs(t.x - x) < 1 && Math.abs(t.y - y) < 1);
    if (i !== -1) {
        text[i].isLeftAligned = true;
        text[i].isIndented = false;
    }
}

export function markIndented(text: TextDetail[], page: number, x: number, y: number) {
    const i = text.findIndex(t => t.page == page && Math.abs(t.x - x) < 1 && Math.abs(t.y - y) < 1);
    if (i !== -1) {
        text[i].isLeftAligned = false;
        text[i].isIndented = true;
    }
}

export class LeftChapterHeading extends PageLayout {
    constructor(pageNum: number) {
        super(pageNum);
    }
    getPage(pdf: PdfData, emit: PageLayoutEmitFunction) {
        const pageContent = pdf.textDetails
            .filter(t => t.page == this.pageNum && ['GoodOT:9', 'SabonLTStd-Roman:9'].includes(t.fontId))
            .map(t => t.y);
        const headingStartY = Math.max(...pageContent) + 25;

        this.getAllTextInBoundingBox(pdf, 0, 520, 750, headingStartY, emit);
        this.getAllTextInBoundingBox(pdf, 76.5, 300, headingStartY, 40, emit);
        this.getAllTextInBoundingBox(pdf, 315, 550, headingStartY, 40, emit);
    }
};

export class RightChapterHeading extends PageLayout {
    constructor(pageNum: number) {
        super(pageNum);
    }
    getPage(pdf: PdfData, emit: PageLayoutEmitFunction) {
        const pageContent = pdf.textDetails
            .filter(t => t.page == this.pageNum && t.fontId == 'SabonLTStd-Roman:9')
            .map(t => t.y);
        const headingStartY = Math.max(...pageContent) + 10;

        this.getAllTextInBoundingBox(pdf, 0, 520, 750, headingStartY, emit);
        this.getAllTextInBoundingBox(pdf, 67.5, 300, headingStartY, 40, emit);
        this.getAllTextInBoundingBox(pdf, 300, 520, headingStartY, 40, emit);
    }
};

export class RightChapterHeadingWithSidebar extends PageLayout {
    leftX: number;

    constructor(pageNum: number, leftX?: number) {
        super(pageNum);
        this.leftX = leftX ?? 67.5;
    }
    getPage(pdf: PdfData, emit: PageLayoutEmitFunction) {
        const pageContent = pdf.textDetails
            .filter(t => t.page == this.pageNum && t.fontId == 'SabonLTStd-Roman:9')
            .map(t => t.y);
        const headingStartY = Math.max(...pageContent) + 10;

        this.getAllTextInBoundingBox(pdf, this.leftX, 520, 750, headingStartY, emit);
        this.getAllTextInBoundingBox(pdf, this.leftX, 400, headingStartY, 40, emit);
        this.getAllTextInBoundingBox(pdf, 400, 520, headingStartY, 40, emit, 'sidebar'+this.pageNum);
    }
};

export class RightChapterHeadingWithSidebarAPG extends PageLayout {
    leftX: number;

    constructor(pageNum: number, leftX?: number) {
        super(pageNum);
        this.leftX = leftX ?? 67.5;
    }
    getPage(pdf: PdfData, emit: PageLayoutEmitFunction) {
        const pageContent = pdf.textDetails
            .filter(t => t.page == this.pageNum && t.fontId == 'SabonLTStd-Roman:9')
            .map(t => t.y);
        const headingStartY = Math.max(...pageContent) + 10;

        this.getAllTextInBoundingBox(pdf, this.leftX, 520, 750, headingStartY, emit);
        this.getAllTextInBoundingBox(pdf, this.leftX, 375, headingStartY, 40, emit);
        this.getAllTextInBoundingBox(pdf, 385.5, 520, headingStartY, 40, emit, 'sidebar'+this.pageNum);
    }
};

export class RightOneColumnWithSidebar extends PageLayout {
    leftX: number;

    constructor(pageNum: number, leftX?: number) {
        super(pageNum);
        this.leftX = leftX ?? 67.5;
    }
    getPage(pdf: PdfData, emit: PageLayoutEmitFunction) {
        this.getAllTextInBoundingBox(pdf, this.leftX, 390, 750, 40, emit);
        this.getAllTextInBoundingBox(pdf, 400, 520, 750, 40, emit, 'sidebar'+this.pageNum);
    }
};

export class RightOneColumnWithSidebarAPG extends PageLayout {
    leftX: number;

    constructor(pageNum: number, leftX?: number) {
        super(pageNum);
        this.leftX = leftX ?? 67.5;
    }
    getPage(pdf: PdfData, emit: PageLayoutEmitFunction) {
        this.getAllTextInBoundingBox(pdf, this.leftX, 375, 750, 40, emit);
        this.getAllTextInBoundingBox(pdf, 385.5, 520, 750, 40, emit, 'sidebar'+this.pageNum);
    }
};

export class RightTwoColumnWithMap extends PageLayout {
    constructor(pageNum: number) {
        super(pageNum);
    }
    getPage(pdf: PdfData, emit: PageLayoutEmitFunction) {
        const pageContent = pdf.textDetails
            .filter(t => t.page == this.pageNum && ['SabonLTStd-Roman:9', 'GoodOT:9'].includes(t.fontId))
            .map(t => t.y);
        const headingStartY = Math.max(...pageContent) + 30;

        this.getAllTextInBoundingBox(pdf, 67.5, 290, headingStartY, 40, emit);
        this.getAllTextInBoundingBox(pdf, 306, 520, headingStartY, 40, emit);
    }
};


export class LeftTwoColumnWithMap extends PageLayout {
    constructor(pageNum: number) {
        super(pageNum);
    }
    getPage(pdf: PdfData, emit: PageLayoutEmitFunction) {
        const pageContent = pdf.textDetails
            .filter(t => t.page == this.pageNum && t.fontId == 'SabonLTStd-Roman:9')
            .map(t => t.y);
        const headingStartY = Math.max(...pageContent) + 10;

        this.getAllTextInBoundingBox(pdf, 76.5, 300, headingStartY, 40, emit);
        this.getAllTextInBoundingBox(pdf, 315, 550, headingStartY, 40, emit);
    }
};

export class LeftTwoColumn extends PageLayout {
    leftX: number;

    constructor(pageNum: number, leftX?: number) {
        super(pageNum);
        this.leftX = leftX ?? 76.5;
    }
    getPage(pdf: PdfData, emit: PageLayoutEmitFunction) {
        this.getAllTextInBoundingBox(pdf, this.leftX, 300, 715, 40, emit);
        this.getAllTextInBoundingBox(pdf, 315, 550, 715, 40, emit);
    }
};

export class LeftTwoColumnWithLeftSidebar extends PageLayout {
    constructor(pageNum: number) {
        super(pageNum);
    }
    getPage(pdf: PdfData, emit: PageLayoutEmitFunction) {
        this.getAllTextInBoundingBox(pdf, 85.8, 300, 715, 40, emit, 'sidebar'+this.pageNum);
        this.getAllTextInBoundingBox(pdf, 315, 550, 715, 40, emit);
    }
};

export class LeftOneColumnWithLeftSidebar extends PageLayout {
    constructor(pageNum: number) {
        super(pageNum);
    }
    getPage(pdf: PdfData, emit: PageLayoutEmitFunction) {
        this.getAllTextInBoundingBox(pdf, 0, 210, 715, 40, emit, 'sidebar'+this.pageNum);
        this.getAllTextInBoundingBox(pdf, 211.5, 550, 715, 40, emit);
    }
};

export class RightTwoColumn extends PageLayout {
    constructor(pageNum: number) {
        super(pageNum);
    }
    getPage(pdf: PdfData, emit: PageLayoutEmitFunction) {
        this.getAllTextInBoundingBox(pdf, 67.5, 290, 715, 40, emit);
        this.getAllTextInBoundingBox(pdf, 306, 525, 715, 40, emit);
    }
};

export class RightTwoColumnWithRightSidebar extends PageLayout {
    constructor(pageNum: number) {
        super(pageNum);
    }
    getPage(pdf: PdfData, emit: PageLayoutEmitFunction) {
        this.getAllTextInBoundingBox(pdf, 67.5, 290, 715, 40, emit);
        this.getAllTextInBoundingBox(pdf, 306, 525, 715, 40, emit, 'sidebar'+this.pageNum);
    }
};

export function detectPageLayout(pdf: PdfData, page: number): string {
    const entries = pdf.textDetails.filter(x => x.page == page);

    const leftCol1 = entries.filter(x => Math.abs(x.x - 76.5) < 1).length > 5;
    const leftCol2 = entries.filter(x => Math.abs(x.x - 315) < 1).length > 5;
    const rightCol1 = entries.filter(x => Math.abs(x.x - 67.5) < 1).length > 5;
    const rightCol2 = entries.filter(x => Math.abs(x.x - 306) < 1).length > 5;
    const leftLeft1 = entries.filter(x => Math.abs(x.x - 211.5) < 1).length > 5;
    const rightSideAPG = entries.filter(x => Math.abs(x.x - 385.5) < 1).length > 5;
    const hasHeading = entries.some(x => x.fontName == 'Taroca' && x.fontSize >= 21 && x.y < 720);

    const topOfText = Math.max(...entries.filter(t => t.fontId == 'SabonLTStd-Roman:9' || t.fontId == 'GoodOT:9').map(t => t.y));

    // right class def / one column pp 40,

    if (rightCol1 && rightCol2) {
        if (hasHeading) {
            return `new Core.RightChapterHeading(${page})`;
        } else if (topOfText > 650) {
            return `new Core.RightTwoColumn(${page})`;
        } else {
            return `new Core.RightTwoColumnWithMap(${page})`;
        }
    } else if (leftCol1 && leftCol2) {
        if (hasHeading) {
            return `new Core.LeftChapterHeading(${page})`;
        } else if (topOfText > 650) {
            return `new Core.LeftTwoColumn(${page})`;
        } else {
            return `new Core.LeftTwoColumnWithMap(${page})`;
        }
    } else if (rightCol1) {
        if (hasHeading) {
            return `new Core.RightChapterHeadingWithSidebar(${page})`;
        } else if (rightSideAPG) {
            return `new Core.RightOneColumnWithSidebarAPG(${page})`;
        } else {
            return `new Core.RightOneColumnWithSidebar(${page})`;
        }
    } else if (leftCol1) {
        if (hasHeading) {
            return `new Core.LeftChapterHeadingWithSidebar(${page})`;
        } else {
            return `new Core.LeftOneColumnWithSidebar(${page})`;
        }
    } else if (leftCol2 && hasHeading) {
        return `new Core.LeftTwoColumnWithLeftSidebar(${page})`;
    } else if (leftLeft1) {
        return `new Core.LeftOneColumnWithLeftSidebar(${page})`;
    }

    return '';
}

const parseIcon = (p: ParserText) => {
    return p.consume().text;
};

const parseItalic = makeContiguousParser("<i>", "</i>");
const parseBold = makeContiguousParser("<b>", "</b>");
const parseHeading = makeContiguousParser("<h3>", "</h3>");

const parseLargeCapSubheading = (p: ParserText) => {
    let heading = '';
    while (p.anyRemaining() && (p.peek().fontId == 'Gin-Regular-SC700:12' || p.peek().fontId == 'Gin-Regular-SC700:8.4')) {
        if (p.peek().fontId == 'Gin-Regular-SC700:12') {
            heading += p.consume().text.toUpperCase();
        } else {
            heading += p.consume().text.toLowerCase();
        }
    }
    return '<h4>' + heading + '</h4>';
}

const headingFontIds = ['GoodOT-News:12', 'GoodOT:12', 'Taroca:24', 'Taroca:23'];
const subheadingFontIds = ['Gin-Regular:12', 'Gin-Regular:14', 'Gin-Round:12'];
const normalFontIds = ['SabonLTStd-Roman:9', 'Sabon-Roman:9', 'Gin-Regular-SC700:9'];
const italicFontIds = ['SabonLTStd-Italic:9', 'TimesNewRomanPS-ItalicMT:9'];
const boldFontIds = ['TimesNewRomanPS-BoldMT:9', 'TimesNewRomanPSMT:9', 'Sabon-Bold:9', 'SabonLTStd-Bold:9', 'Times-Bold:9'];

const paragraphFontIds = headingFontIds
    .concat(normalFontIds)
    .concat(italicFontIds)
    .concat(boldFontIds);


const parseParagraph = (p: ParserText) => {
    let current = '';

    while (p.anyRemaining()) {
        const peekFont = p.peek().fontId;
        if (normalFontIds.includes(peekFont)) {
            current += p.consume().text;
        } else if (headingFontIds.includes(peekFont)) {
            current += parseHeading(p);
        } else if (italicFontIds.includes(peekFont)) {
            current += parseItalic(p);
        } else if (boldFontIds.includes(peekFont)) {
            current += parseBold(p);
        } else if (p.peek().text == "’") {
            current += p.consume().text;
        } else {
            break;
        }

        if (p.anyRemaining() && p.peek().isIndented) {
            break;
        }
    }
    return "<p>" + current + "</p>";
};

const parseBlockquote = (p: ParserText) => {
    let current = '';
    while (p.anyRemaining()) {
        if (p.peek().fontId == 'GoodOT:9') {
            current += p.consume().text;
        } else if (p.peek().fontId == 'GoodOT-Italic:9') {
            current += parseItalic(p);
        } else if (p.peek().fontId == 'GoodOT-Bold:9') {
            current += parseBold(p);
        } else {
            break;
        }
    }
    return "<blockquote>" + current + "</blockquote>";
};

const parseHeadingBlockquote = (p: ParserText) => {
    let current = '';
    while (p.anyRemaining()) {
        if (p.peek().fontId == 'SabonLTStd-Italic:12') {
            current += p.consume().text;
        } else if (p.peek().fontId == 'SabonLTStd-Roman:12') {
            current += parseItalic(p);
        } else {
            break;
        }
    }
    return "<blockquote>" + current + "</blockquote>";
};

const parseList = (p: ParserText) => {
    let current = '';

    p.consume();
    
    while (p.anyRemaining()) {
        const peekFont = p.peek().fontId;
        if (normalFontIds.includes(peekFont)) {
            current += p.consume().text;
        } else if (headingFontIds.includes(peekFont)) {
            current += parseHeading(p);
        } else if (italicFontIds.includes(peekFont)) {
            current += parseItalic(p);
        } else if (boldFontIds.includes(peekFont)) {
            current += parseBold(p);
        } else {
            break;
        }

        if (p.anyRemaining() && p.peek().isIndented) {
            break;
        }
    }
    return "<li>" + current + "</li>";
};

const parseAside = (p: ParserText) => {
    let headingFont = p.peek().fontId;
    let heading = p.consume().text;
    while (p.anyRemaining() && p.peek().fontId == headingFont)
        heading += p.consume().text;

    return new Journal(heading, parseBlockquote(p));
};

const parseSection = (p: ParserText, moduleData: ModuleData) => {
    let current = '';

    let name = p.consume().text;
    if (p.anyRemaining() && p.peek().fontId == 'GoodOT-Bold:14') {
        name += "(" + p.consume().text + ")";
    }

    const headingFonts = ['Taroca:16', 'GoodOT-Bold:12'];
    while (p.anyRemaining() && headingFonts.includes(p.peek().fontId)) {
        name += p.consume().text;
    }

    while (p.anyRemaining()) {
        let cur = p.peek();
        if (cur.fontId == 'GoodOT:9' && cur.text == '•') {
            current += parseList(p);
        } else if (cur.fontId == 'GoodOT:9' || cur.fontId == 'GoodOT-Bold:9') {
            current += parseBlockquote(p);
        } else if (cur.fontId == 'GoodOT-CondBold:12') {
            current += parseMonsterOrHazard(p, moduleData);
        } else if (cur.fontId == 'GoodOT-Bold:11') {
            moduleData.journalEntries.push(parseAside(p));
        } else if (cur.fontId == 'SabonLTStd-Italic:12') {
            current += parseHeadingBlockquote(p);
        } else if (cur.fontId == 'GoodOT-Bold:12' && !(cur.text.match(/^[ABCDEFG]\d+\./) || cur.text.match(/^Event \d+:/))) {
            current += parseHeading(p);
        } else if (subheadingFontIds.includes(cur.fontId)) {
            current += parseHeading(p);
        } else if (cur.fontId == 'Gin-Regular-SC700:12') {
            current += parseLargeCapSubheading(p);
        } else if (paragraphFontIds.includes(cur.fontId)) {
            current += parseParagraph(p);
        } else if (cur.fontId == 'GoodOT-CondBold:7') {
            current += `<span style="border: 1px #000000 solid; padding: 0.2em">${p.consume().text}</span>`;
        } else {
            //console.log(`abandoning section on: ${cur.fontId} :: (${cur.x}, ${cur.y}) :: ${cur.text}`);
            break;
        }
    }

    moduleData.journalEntries.push(new Journal(name, current));
};

const parseHazardLine = (p: ParserText, endOnIndented: boolean = false) => {
    let current = '';

    let isFirstElement = true;
    while (p.anyRemaining()) {
        if (endOnIndented) {
            if (!isFirstElement && p.peek().isIndented)
                break;
        } else {
            if (!isFirstElement) {
                if (p.peek().isLeftAligned
                && !['Critical Success', 'Success', 'Failure', 'Critical Failure'].includes(p.peek().text.trim()))
                break;
            }
        }
        isFirstElement = false;

        if (p.peek().fontId == 'GoodOT:9') {
            current += p.consume().text;
        } else if (p.peek().fontId == 'Dax-Regular:9') {
            current += p.consume().text;
        } else if (p.peek().fontId == 'GoodOT-Italic:9') {
            current += parseItalic(p);
        } else if (p.peek().fontId == 'GoodOT-Bold:9' || p.peek().fontId == 'Dax-Bold:9') {
            current += parseBold(p);
        } else if (p.peek().fontId == 'Pathfinder-Icons:9') {
            current += parseIcon(p);
        } else {
            break;
        }
    }

    return current;
}

const parseAbility = (data: string) => {
    const value = parseInt(data, 10);
    return {
        value: value * 2 + 10,
        mod: value
    };
}

export const parseSpell = (p: ParserText, moduleData: ModuleData, name: string, level: number, tags: string[], qty: number) => {
    let spell = new SpellItem();
    
    //console.log('------'+name+'------');

    spell.name = name;
    spell.data.level.value = level;
    spell.data.spellCategorie.value = 'spell';

    for (let trait of tags) {
        trait = trait.toLocaleLowerCase().trim();

        spell.data.traits.value.push(trait);
    }

    while (p.anyRemaining()) {
        let next = p.peek();
        if (!['GoodOT:9', 'Dax-Regular:9', 'GoodOT-Italic:9', 'GoodOT-Bold:9'].includes(next.fontId))
            break;

        let line = parseHazardLine(p)
            .replace(/–/g, '-');

        let match;
        if (match = line.match(/^<b>Traditions?<\/b> (.*)$/)) {
            spell.data.traditions.value = match[1].split(',').map(x => x.toLocaleLowerCase().trim());
        } else if (match = line.match(/^<b>Cast<\/b> ?\[(.*)\] ?(.*)$/)) {
            switch (match[1]) {
                case 'one-action': spell.data.time.value = '1'; break;
                case 'two-actions': spell.data.time.value = '2'; break;
                case 'three-actions': spell.data.time.value = '3'; break;
                case 'reaction': spell.data.time.value = 'reaction'; break;
                default: console.log('unknown cast time: ' + match[1]); break;
            }
            spell.data.components.value = match[2];
        } else if (match = line.match(/^<b>Cast<\/b> ?(.*) \((.*)\)$/)) {
            spell.data.time.value = match[1];
            spell.data.components.value = match[2];
        } else if (match = line.match(/^<b>Cast<\/b> ?\((.*)\) (.*)$/)) {
            spell.data.time.value = match[1];
            spell.data.components.value = match[2];
        } else if (match = line.match(/^<b>Range<\/b> ?(.*); <b>Targets<\/b> (.*)/)) {
            spell.data.range.value = match[1];
            spell.data.target.value = match[2];
        } else if (match = line.match(/^<b>Range<\/b> ?(.*)/)) {
            spell.data.range.value = match[1];
        } else if (match = line.match(/^<b>Saving ?Throw<\/b> ?(basic)? ?(.*?)(; ?(<b> ?Duration<\/b> (.*)))?$/)) {
            if (match[1] == 'basic') {
                spell.data.save.basic = 'basic';
            }
            switch (match[2]) {
                case 'Fort': case 'Fortitude': spell.data.save.value = 'fort'; break;
                case 'Ref': case 'Reflex': spell.data.save.value = 'reflex'; break;
                case 'Will': spell.data.save.value = 'will'; break;
                default: console.log('unknown save type: ' + match[2]); break;
            }
            if (match[5]) {
                spell.data.duration.value = match[5];
            }
        } else if (match = line.match(/^<b>Duration<\/b> ?(.*)/)) {
            spell.data.duration.value = match[1];
        } else if (match = line.match(/^<b>Area<\/b> ?(.*)/)) {
            spell.data.area.value = match[1];
        } else {
            //console.log("unknown spell line (" + name + "): "+line);
            if (spell.data.description.value != '' && line.startsWith('<')) spell.data.description.value += '<br>';
            spell.data.description.value += line;
        }
    }

    moduleData.items.push(spell);

    if (qty !== undefined) {
        name += " (" + qty + ")";
    }
    return `<h4>${name} - Spell ${level}</h4>`;
};


export const parseMonster = (p: ParserText, moduleData: ModuleData, name: string, level: number, tags: string[], qty: number) => {
    let npc = new NPCActor();
    
    //console.log('------'+name+'------');

    npc.name = name;
    npc.data.details.level.value = level;

    for (let trait of tags) {
        trait = trait.toLocaleLowerCase().trim();

        if (['LG', 'LN', 'LE', 'NG', 'N', 'NE', 'CG', 'CN', 'CE'].includes(trait.toLocaleUpperCase())) {
            npc.data.details.alignment.value = trait.toLocaleUpperCase();
        } else if (trait == 'tiny') {
            npc.data.traits.size.value = 'tiny';
        } else if (trait == 'small') {
            npc.data.traits.size.value = 'sm';
        } else if (trait == 'medium') {
            npc.data.traits.size.value = 'med';
        } else if (trait == 'large') {
            npc.data.traits.size.value = 'lg';
        } else if (trait == 'huge') {
            npc.data.traits.size.value = 'huge';
        } else if (trait == 'gargantuan') {
            npc.data.traits.size.value = 'grg';
        } else {
            npc.data.traits.traits.value.push(trait);
        }
    }

    while (p.anyRemaining()) {
        let next = p.peek();
        if (!['GoodOT:9', 'Dax-Regular:9', 'GoodOT-Italic:9', 'GoodOT-Bold:9'].includes(next.fontId))
            break;

        let line = parseHazardLine(p)
            .replace(/–/g, '-');

        let match, dmgMatch;
        if (match = line.match(/^<b>Perception<\/b> ([+-]?\d+)(; (.*))?$/)) {
            npc.data.attributes.perception.value = parseInt(match[1], 10);
            npc.data.traits.senses.value = match[3];
        } else if (match = line.match(/^<b>Languages<\/b> (.*)$/)) {
            npc.data.traits.languages.value = match[1].split(',').map(x => x.trim());
        } else if (match = line.match(/^<b>Skills<\/b> (.*)$/)) {
            const skills = match[1].split(',');
            for (const skill of skills) {
                const skillMatch = skill.trim().match(/(.*) ([+-]\d+)/);
                if (!skillMatch) continue;

                const lore = new LoreItem();
                lore.name = skillMatch[1];
                lore.data.mod.value = parseInt(skillMatch[2], 10);
                npc.items.push(lore);
            }
        } else if (match = line.match(/^<b>Skills<\/b> (.*)$/)) {
            const skills = match[1].split(',');
            for (const skill of skills) {
                const skillMatch = skill.trim().match(/(.*) ([+-]\d+)/);
                if (!skillMatch) continue;

                const lore = new LoreItem();
                lore.name = skillMatch[1];
                lore.data.mod.value = parseInt(skillMatch[2], 10);
                npc.items.push(lore);
            }
        } else if (match = line.match(/^<b>Items<\/b> (.*)$/)) {
            const items = match[1].split(',');
            for (const item of items) {
                const itemMatch = item.trim().match(/^(.*?)( \((\d+)\))?$/);
                if (!itemMatch) continue;

                const equipmentItem = new EquipmentItem();
                equipmentItem.name = itemMatch[1];
                equipmentItem.data.description.value = '';
                if (itemMatch[3]) {
                    equipmentItem.data.quantity.value = parseInt(itemMatch[3], 10);
                }
                npc.items.push(equipmentItem);
            }
        } else if (match = line.match(/^<b>Str<\/b> ([+-]?\d+), <b>Dex<\/b> ([+-]?\d+), <b>Con<\/b> ([+-]?\d+), <b>Int<\/b> ([+-]?\d+), <b>Wis<\/b> ([+-]?\d+), <b>Cha<\/b> ([+-]?\d+)$/)) {
            npc.data.abilities.str = parseAbility(match[1]);
            npc.data.abilities.dex = parseAbility(match[2]);
            npc.data.abilities.con = parseAbility(match[3]);
            npc.data.abilities.int = parseAbility(match[4]);
            npc.data.abilities.wis = parseAbility(match[5]);
            npc.data.abilities.cha = parseAbility(match[6]);
        } else if (match = line.match(/^<b>AC<\/b> ([+-]?\d+); <b>Fort<\/b> ([+-]?\d+), <b>Ref<\/b> ([+-]?\d+), <b>Will<\/b> ([+-]?\d+)/)) {
            npc.data.attributes.ac.value = parseInt(match[1], 10);
            npc.data.saves.fortitude.value = parseInt(match[2], 10);
            npc.data.saves.reflex.value = parseInt(match[3], 10);
            npc.data.saves.will.value = parseInt(match[4], 10);
        } else if (match = line.match(/^<b>HP<\/b> ([+-]?\d+)/)) {
            const hp = parseInt(match[1], 10);
            npc.data.attributes.hp.value = hp;
            npc.data.attributes.hp.max = hp;
        } else if (match = line.match(/^<b>Speed<\/b> (.+)$/)) {
            npc.data.attributes.speed.value = match[1];
        } else if (match = line.match(/^<b>(Melee|Ranged)<\/b> ?(\[one-action\])? ?(.*) (\+?\d+)( \((.*)\))?, (<b>Damage<\/b> ?(.*?))?(<b>Effect<\/b> (.*?))?(; no multiple attack penalty)?$/)) {
            let attack = new MeleeItem();
            if (match[1] == "Ranged") attack.data.weaponType.value = "ranged";
            attack.name = match[3];
            attack.data.bonus.value = parseInt(match[4], 10);

            if (match[8] && (dmgMatch = match[8].match(/^(.*?) (\w+)( plus (.*?))?$/))) {
                attack.data.damageRolls.push({ damage: dmgMatch[1], damageType: dmgMatch[2] });
                if (dmgMatch[4]) {
                    attack.data.attackEffects.value.push(dmgMatch[4]);
                }
            }
            
            if (match[10]) {
                attack.data.attackEffects.value.push(match[10].trim());
            }

            if (match[11]) {
                attack.data.attackEffects.value.push("no multiple attack penalty");
            }

            npc.items.push(attack);
        } else if (match = line.match(/<b>(.*?)<\/b> ?\[(reaction|free-action)\]( \((.*)\);? )?(<b>Trigger<\/b>.*<b>Effect ?<\/b>.*)/)) {
            let reaction = new ActionItem();
            if (match[2] == 'reaction') {
                reaction.data.actionType.value = 'reaction';
                reaction.img = 'systems/pf2e/icons/actions/Reaction.png';
            } else if (match[2] == 'free-action') {
                reaction.data.actionType.value = 'free';
                reaction.img = 'systems/pf2e/icons/actions/FreeAction.png';
            }
            reaction.name = match[1];
            reaction.data.description.value = match[5];
            if (match[4]) {
                let traits = match[4].split(',');
                for (let trait of traits)
                    reaction.data.traits.value.push(trait.trim());
            }
            npc.items.push(reaction);            
        } else if (match = line.match(/<b>(.*?)<\/b> ?\[(one-action|two-actions)\]( \((.*)\);? )?(.*)/)) {
            let action = new ActionItem();
            if (match[2] == 'one-action') {
                action.data.actionType.value = 'action';
                action.data.actions.value = '1';
                action.img = 'systems/pf2e/icons/actions/OneAction.png';
            } else if (match[2] == 'two-actions') {
                action.data.actionType.value = 'action';
                action.data.actions.value = '2';
                action.img = 'systems/pf2e/icons/actions/TwoActions.png';
            }
            action.name = match[1];
            action.data.description.value = match[5];
            if (match[4]) {
                let traits = match[4].split(',');
                for (let trait of traits)
                    action.data.traits.value.push(trait.trim());
            }
            npc.items.push(action);
        } else if (match = line.match(/<b>(.*?)<\/b>( \((.*?)\);? )?(.*)$/)) {
            //console.log(line);
            let passive = new ActionItem();
            passive.data.actionType.value = "passive";
            passive.img = 'systems/pf2e/icons/actions/Passive.png';
            passive.name = match[1].trim();
            passive.data.description.value = match[4].trim();
            if (match[3]) {
                let traits = match[3].split(',');
                for (let trait of traits)
                    passive.data.traits.value.push(trait.trim());
            }
            npc.items.push(passive);
        } else {
            console.log("unknown npc line (" + name + "): "+line);
        }
    }

    moduleData.actors.push(npc);

    if (qty !== undefined) {
        name += " (" + qty + ")";
    }
    return `<h4>${name} - Creature ${level}</h4>`;
};

export const parseFeat = (p: ParserText, moduleData: ModuleData, name: string, level: number, tags: string[], qty: number) => {
    let feat = new FeatItem();

    if (name.indexOf('[free-action]') !== -1) {
        name = name.replace('[free-action]', '');
        feat.data.actionType.value = 'free';
    }
    if (name.indexOf('[reaction]') !== -1) {
        name = name.replace('[reaction]', '');
        feat.data.actionType.value = 'reaction';
    }
    if (name.indexOf('[one-action]') !== -1) {
        name = name.replace('[one-action]', '');
        feat.data.actionType.value = 'action';
        feat.data.actions.value = '1';
    }
    if (name.indexOf('[two-actions]') !== -1) {
        name = name.replace('[two-actions]', '');
        feat.data.actionType.value = 'action';
        feat.data.actions.value = '2';
    }
    if (name.indexOf('[three-actions]') !== -1) {
        name = name.replace('[three-actions]', '');
        feat.data.actionType.value = 'action';
        feat.data.actions.value = '3';
    }
    name = name.trim();

    feat.name = name;
    feat.data.level.value = level;

    for (let trait of tags) {
        trait = trait.toLocaleLowerCase();
        feat.data.traits.value.push(trait);
    }

    if (feat.data.traits.value.some(tag => ancestries.includes(tag))) {
        feat.data.featType.value = 'ancestry';
    } else if (feat.data.traits.value.some(tag => classes.includes(tag))) {
        feat.data.featType.value = 'class';
    } else {
        feat.data.featType.value = 'general';
    }
    

    let heading = '';
    // parse out prerequisite
    if (p.anyRemaining() && p.peek().text.trim() == 'Prerequisites') {
        p.consume();
        feat.data.prerequisites.value = parseHazardLine(p).trim();
    }

    // parse out frequency
    if (p.anyRemaining() && p.peek().text.trim() == 'Frequency') {
        if (heading != '') heading += '<br>';
        heading += parseHazardLine(p).trim();
    }

    // parse out trigger
    if (p.anyRemaining() && p.peek().text.trim() == 'Trigger') {
        if (heading != '') heading += '<br>';
        heading += parseHazardLine(p).trim();
    }

    let current = '';
    while (p.anyRemaining()) {
        let next = p.peek();
        if (!['GoodOT:9', 'Dax-Regular:9', 'GoodOT-Italic:9', 'GoodOT-Bold:9'].includes(next.fontId))
            break;

        let line = parseHazardLine(p, true);
        if (current != '') current += '<br>';
        current += line;
    }
    

    if (heading != '')
        current = heading + '<hr>' + current;
    feat.data.description.value = current;

    moduleData.items.push(feat);

    return `<h4>${name} - Feat ${level}</h4>`;
}

export const parseHazard = (p: ParserText, moduleData: ModuleData, name: string, level: number, tags: string[], qty: number) => {
    let hazard = new HazardActor();

    hazard.name = name;
    hazard.data.details.level = level;

    for (let trait of tags) {
        trait = trait.toLocaleLowerCase();
        if (trait == 'trap')
            continue;
        if (trait == 'complex') {
            hazard.data.details.isComplex = true;
            continue;
        }            
        hazard.data.traits.traits.value.push(trait);
    }

    let current = '';
    while (p.anyRemaining()) {
        let next = p.peek();
        if (!['GoodOT:9', 'Dax-Regular:9', 'GoodOT-Italic:9', 'GoodOT-Bold:9'].includes(next.fontId))
            break;

        let line = parseHazardLine(p);

        line = line.replace('(arcane, conjuration, summon;', '(arcane, conjuration, summon);');

        let match, dmgMatch;
        if (match = line.match(/^<b>Stealth<\/b> ?DC (\d+)( (.*))?$/)) {
            hazard.data.attributes.stealth.value = parseInt(match[1], 10) - 10;
            hazard.data.attributes.stealth.details = match[3];
        } else if (match = line.match(/^<b>Stealth<\/b> ?(\+\d+) (.*)$/)) {
            hazard.data.attributes.stealth.value = parseInt(match[1], 10);
            hazard.data.attributes.stealth.details = match[2];
        } else if (match = line.match(/^<b>Description<\/b> ?(.*)$/)) {
            hazard.data.details.description = match[1];
        } else if (match = line.match(/^<b>Disable<\/b> ?(.*)$/)) {
            hazard.data.details.disable = match[1];
        } else if (match = line.match(/^<b>Routine<\/b> ?(.*)$/)) {
            hazard.data.details.routine = match[1];
        } else if (match = line.match(/^<b>Reset<\/b> ?(.*)$/)) {
            hazard.data.details.reset = match[1];
        } else if (match = line.match(/^<b>AC<\/b> ?(\d+); <b>Fort<\/b> ?(\+?\d+),? <b>Ref<\/b> ?(\+?\d+)(,? <b>Will<\/b> ?(\+?\d+))?$/)) {
            hazard.data.attributes.hasHealth = true;
            hazard.data.attributes.ac.value = parseInt(match[1], 10);
            hazard.data.saves.fortitude.value = parseInt(match[2], 10);
            hazard.data.saves.reflex.value = parseInt(match[3], 10);
            if (match[5]) {
                hazard.data.saves.will.value = parseInt(match[5], 10);
            }
        } else if (match = line.match(/^(<b>.*?Hardness<\/b> (\d+), )?<b>.*?HP ?<\/b> ?(\d+)( \(BT \d+\))?(.*?)(; <b>Immunities<\/b> ?(.*))?$/)) {
            hazard.data.attributes.hasHealth = true;
            hazard.data.attributes.hardness = parseInt(match[2]??'0', 10) ?? 0;
            hazard.data.attributes.hp.value = parseInt(match[3], 10);
            hazard.data.attributes.hp.max = parseInt(match[3], 10);
            if (match[5]) {
                hazard.data.attributes.hp.details = match[5].trim();
            }
            let immunities = (match[7] || '').split(',');
            for (let immunity of immunities) {
                hazard.data.traits.di.value.push(immunity.trim().replace(' ', '-'));
            }
        } else if (match = line.match(/^<b>(Melee|Ranged)<\/b>(\[one-action\])? (.*) (\+?\d+)( \((.*)\))?, (<b>Damage<\/b> ?(.*?))?(<b>Effect<\/b> (.*?))?(; no multiple attack penalty)?$/)) {
            let attack = new MeleeItem();
            if (match[1] == "Ranged") attack.data.weaponType.value = "ranged";
            attack.name = match[3];
            attack.data.bonus.value = parseInt(match[4], 10);

            if (match[8] && (dmgMatch = match[8].match(/^(.*?) (\w+)( plus (.*?))?$/))) {
                attack.data.damageRolls.push({ damage: dmgMatch[1], damageType: dmgMatch[2] });
                if (dmgMatch[4]) {
                    attack.data.attackEffects.value.push(dmgMatch[4]);
                }
            }
            
            if (match[10]) {
                attack.data.attackEffects.value.push(match[10].trim());
            }

            if (match[11]) {
                attack.data.attackEffects.value.push("no multiple attack penalty");
            }

            hazard.items.push(attack);
        } else if (match = line.match(/<b>(.*?)<\/b> ?\[(reaction|free-action)\]( \((.*)\);? )?(<b>Trigger<\/b>.*<b>Effect ?<\/b>.*)/)) {
            let reaction = new ActionItem();
            if (match[2] == 'reaction') {
                reaction.data.actionType.value = "reaction";
                reaction.img = 'systems/pf2e/icons/actions/Reaction.png';
            } else if (match[2] == 'free-action') {
                reaction.data.actionType.value = 'free';
                reaction.img = 'systems/pf2e/icons/actions/FreeAction.png';
            }
            reaction.name = match[1];
            reaction.data.description.value = match[5];
            if (match[4]) {
                let traits = match[4].split(',');
                for (let trait of traits)
                    reaction.data.traits.value.push(trait.trim());
            }
            hazard.items.push(reaction);
        } else if (match = line.match(/<b>(.*?)<\/b>( \((.*?)\);? )?(.*)$/)) {
            //console.log(line);
            let passive = new ActionItem();
            passive.data.actionType.value = "passive";
            passive.img = 'systems/pf2e/icons/actions/Passive.png';
            passive.name = match[1].trim();
            passive.data.description.value = match[4].trim();
            if (match[3]) {
                let traits = match[3].split(',');
                for (let trait of traits)
                    passive.data.traits.value.push(trait.trim());
            }
            hazard.items.push(passive);
        } else {
            console.log(name + ":: " + line);
        }
    }

    if (name != 'Hazard Name')
    {
        moduleData.actors.push(hazard);
    }

    if (qty !== undefined) {
        name += " (" + qty + ")";
    }
    return `<h4>${name} - Hazard ${level}</h4>`;
};

const parseMonsterReference = (p: ParserText, name: string, difficulty: string, tags: string[], qty: number) => {
    let current = '';
    let isFirst = true;
    while (p.anyRemaining()) {
        if (!isFirst && p.peek().isLeftAligned && p.peek().fontId == 'GoodOT-Bold:9') {
            current += "<br>";
        }

        if (p.peek().fontId == 'GoodOT:9' || p.peek().fontId == 'GoodOT-CondBold:9') {
            current += p.consume().text;
        } else if (p.peek().fontId == 'Dax-Regular:9') {
            current += p.consume().text;
        } else if (p.peek().fontId == 'GoodOT-Italic:9') {
            current += parseItalic(p);
        } else if (p.peek().fontId == 'GoodOT-Bold:9') {
            current += parseBold(p);
        } else if (p.peek().fontId == 'Pathfinder-Icons:9') {
            current += parseIcon(p);
        } else {
            break;
        }

        isFirst = false;
    }

    if (qty !== undefined) {
        name += " (" + qty + ")";
    }

    if (difficulty !== undefined && difficulty.length > 0) {
        name += " - " + difficulty;
    }

    const tagsHtml = tags.map(t => `<span style="border: 1px #000000 solid; padding: 0.2em">${t}</span>`).join('');
    return `<blockquote><h4>${name}</h4>${tagsHtml}<br>${current}</blockquote>`;
};

export const parseMonsterOrHazard = (p: ParserText, moduleData: ModuleData) => {
    let name = p.consume().text;
    // there's an empty textblock after the name for some reason)
    while (p.peek().fontId == 'GoodOT-CondBold:12' || p.peek().fontId == 'Pathfinder-Icons:12' || p.peek().fontId == 'Pathfinder-Icons:13')
        name += ' ' + p.consume().text;

    name = name.replace('–', '-')
        .replace(/ +/g, ' ')
        .trim();

    let difficulty = '';
    let match;
    if (match = name.match(/^(.*)((HAZARD|CREATURE|ITEM|FEAT|SPELL) -?\d+)$/i)) {
        name = match[1];
        difficulty = match[2];
    }

    name = sentenceCase(name);

    const levelStr = difficulty
        .replace('HAZARD ', '')
        .replace('CREATURE ', '')
        .replace('ITEM ', '')
        .replace('FEAT ', '')
        .replace('SPELL ', '');
    let level = parseInt(levelStr, 10) ?? 0

    let qty;
    const qtyMatch = name.match(/(.*?) \((.*)\)/);
    if (qtyMatch) {
        name = qtyMatch[1];
        qty = qtyMatch[2];
    }

    let tags = [];
    while (p.anyRemaining() && p.peek().fontId == 'GoodOT-CondBold:7') {
        tags.push(p.consume().text);
    }

    if (tags.length == 0 && !difficulty.toUpperCase().startsWith("FEAT")) {
        // sometimes there's "reference" hazards
        return parseMonsterReference(p, name, difficulty, tags, qty);
    }

    if (difficulty.toUpperCase().startsWith("HAZARD")) {
        return parseHazard(p, moduleData, name, level, tags, qty);
    } else if (difficulty.toUpperCase().startsWith("CREATURE")) {
        return parseMonster(p, moduleData, name, level, tags, qty);
    } else if (difficulty.toUpperCase().startsWith("FEAT")) {
        return parseFeat(p, moduleData, name, level, tags, qty);
    } else if (difficulty.toUpperCase().startsWith("SPELL")) {
        return parseSpell(p, moduleData, name, level, tags, qty);
    } else {
        return parseMonsterReference(p, name, difficulty, tags, undefined);
    }
};

export function extractDetails(text: Array<TextDetail>, moduleData: ModuleData): void {
    const blacklistedFonts = ['Times-Italic:8', 'RomicStd-Bold:12', 'TradeGothicLTStd-BdCn20:12', 'TradeGothicLTStd-BdCn20:8', 'Taroca:12', 'TradeGothicLTStd-BdCn20:11.0714']
    text = text.filter(t => !blacklistedFonts.includes(t.fontId));

    let p = new ParserText(text);

    const headingFonts = ['GoodOT-Bold:14', 'RomicStd-Bold:14', 'RomicStd-Bold:32', 'RomicStd-Bold:33', 'RomicStd-Bold:38', 'RomicStd-Bold:40', 'GoodOT-Bold:11', 'Taroca:16', 'Taroca:24', 'Taroca:23', 'Taroca:21', 'Taroca:30'];

    while (p.anyRemaining()) {
        let cur = p.peek();

        if (cur.fontId == 'GoodOT-Bold:11') {
            moduleData.journalEntries.push(parseAside(p));
        } else if (headingFonts.includes(cur.fontId)) {
            //console.log("starting section: " + cur.text);
            parseSection(p, moduleData);
        // } else if (cur.fontId == 'GoodOT-CondBold:12') {
        //     moduleData.journalEntries.push(new Journal('', parseMonsterOrHazard(p, moduleData)));
        } else {
            let item = p.consume();
            console.log(`unknown ${item.fontId} :: ${item.page} (${item.x}, ${item.y}) :: ${item.text}`);
        }
    }
}
